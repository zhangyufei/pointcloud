//
//  Mesh.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/2/19  5:43 PM.
//    
//  

import MetalKit

struct Mesh {
    let mdlMesh: MDLMesh
    let mtkMesh: MTKMesh
    let submeshes: [Submesh]
    
    init(mdlMesh: MDLMesh, mtkMesh: MTKMesh) {
        self.mdlMesh = mdlMesh
        self.mtkMesh = mtkMesh
        var textures = [String : Submesh.Textures]()
        var submeshes = [Submesh]()
        for item in zip(mdlMesh.submeshes!, mtkMesh.submeshes) {
            let texture: Submesh.Textures
            if let name = Submesh.filename(mdlSubmesh: item.0 as? MDLSubmesh) {
                if let t = textures[name] {
                    texture = t
                } else {
                    texture = Submesh.Textures(filename: name)
                    textures[name] = texture
                }
            } else {
                texture = Submesh.Textures()
            }
            submeshes.append(Submesh(mtkSubmesh: item.1, textures: texture))
        }
        self.submeshes = submeshes
    }
}
