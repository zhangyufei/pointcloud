//
//  Sphere.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/3/10  4:42 PM.
//    
//  

import MetalKit
import SceneKit
import SceneKit.SCNNode

class Sphere: Primitive {
    
    let radius: Float
    let texture: MTLTexture?
    init(device: MTLDevice, radius: Float) {
        self.radius = radius
        texture = try? Primitive.loadTexture(imageName: "1604131731199152.jpg")
        super.init(device: device)
        vertexFunctionName = "vertex_sphere_shader"
        fragmentFunctionName = "fragment_sphere_shader"
        vertexDescriptor = Sphere.Vertex.vertexDescriptor
    }
    
    override func buildBuffers() {
        assert(!vertices.isEmpty, "顶点数据不能为空")
        let vertices = self.vertices as! [Sphere.Vertex]
        vertexBuffer = device.makeBuffer(bytes: vertices,
                                         length: vertices.count *
                                            MemoryLayout<Sphere.Vertex>.stride,
                                         options: [])
        
        if !indices.isEmpty {
            indexBuffer = device.makeBuffer(bytes: indices,
                                            length: indices.count * MemoryLayout<UInt16>.size,
                                            options: [])
        }
    }
    
    override func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        guard let indexBuffer = indexBuffer,
              let pipelineState = pipelineState else { return }
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        withUnsafePointer(to: uniforms) { (p) in
            commandEncoder.setVertexBytes(UnsafeRawPointer(p),
                                          length: MemoryLayout<Uniforms>.stride,
                                          index: 1)
        }
        commandEncoder.setFragmentSamplerState(samplerState, index: 0)
        commandEncoder.setFragmentTexture(texture, index: 0)
        commandEncoder.setFrontFacing(.counterClockwise)
        commandEncoder.setCullMode(.back)
        commandEncoder.setTriangleFillMode(fillMode)
        commandEncoder.drawIndexedPrimitives(type: .triangle,
                                             indexCount: indices.count,
                                             indexType: .uint16,
                                             indexBuffer: indexBuffer,
                                             indexBufferOffset: 0)
    }
    
    override func hitTestWithSegment(ray: Ray, transform: matrix_float4x4) -> Float? {
        guard isUserInteractionEnabled else { return nil }
        
        let localRay = transform.inverse * ray
        
        let radius2 = radius * radius
        if (radius2 == 0) { return nil }
        let L = -localRay.origin
        let tca = simd_dot(L, localRay.direction)
        
        let d2 = simd_dot(L, L) - tca * tca
        if (d2 > radius2) { return nil }
        let thc = sqrt(radius2 - d2)
        var t0, t1: Float
        t0 = tca - thc
        t1 = tca + thc
        
        if (t0 > t1) { swap(&t0, &t1) }
        
        if t0 < 0 {
            t0 = t1
            if t0 < 0 { return nil }
        }
        
        var intersection = localRay.extrapolate(t0)
        intersection = (transform * SIMD4<Float>(intersection, 1)).xyz
        let distance = ray.interpolate(intersection)
        return distance < 0 ? nil : distance
    }
    
    private func hitTest1(ray: Ray, transform: matrix_float4x4) -> Float? {
        let rayT = transform.inverse * ray
        let vertices = self.vertices as! [Sphere.Vertex]
        for i in (0..<indices.count / 3) {
            let v0 = vertices[Int(indices[i*3])].position
            let v1 = vertices[Int(indices[i*3+1])].position
            let v2 = vertices[Int(indices[i*3+2])].position
            
            let (intersect, k) = intersectTriangle(ray: rayT, v0: v0, v1: v1, v2: v2)
            if intersect, let t = k?.t {
                var p = rayT.extrapolate(t)
                p = (modelMatrix * SIMD4<Float>(p, 1)).xyz
                let distance = ray.interpolate(p)
                return distance
            }
        }
        return nil
    }
}


extension Sphere {
    struct Vertex {
        var position: SIMD3<Float>
        var color: SIMD4<Float>
        var textureCoordinate: SIMD2<Float>
        
        static var vertexDescriptor: MTLVertexDescriptor = {
            let vertexDescriptor = MTLVertexDescriptor()
            
            vertexDescriptor.attributes[0].format = .float3
            vertexDescriptor.attributes[0].offset = 0
            vertexDescriptor.attributes[0].bufferIndex = 0
            
            vertexDescriptor.attributes[1].format = .float4
            vertexDescriptor.attributes[1].offset = MemoryLayout<SIMD3<Float>>.stride
            vertexDescriptor.attributes[1].bufferIndex = 0
            
            vertexDescriptor.attributes[2].format = .float2
            vertexDescriptor.attributes[2].offset = MemoryLayout<SIMD3<Float>>.stride + MemoryLayout<SIMD4<Float>>.stride
            vertexDescriptor.attributes[2].bufferIndex = 0
            
            vertexDescriptor.layouts[0].stride = MemoryLayout<Vertex>.stride
            
            return vertexDescriptor
        }()
    }
}

extension Sphere {
    
    func testIntersectTriangle(ray: Ray) -> (Bool, Float?) {
        let rayT = modelMatrix.inverse * ray
        let vertices = self.vertices as! [Sphere.Vertex]
        for i in (0..<indices.count / 3) {
            let v0 = vertices[Int(indices[i*3])].position
            let v1 = vertices[Int(indices[i*3+1])].position
            let v2 = vertices[Int(indices[i*3+2])].position
            
            let (intersect, k) = intersectTriangle(ray: rayT, v0: v0, v1: v1, v2: v2)
            if intersect, let t = k?.t {
                var p = rayT.extrapolate(t)
                p = (modelMatrix * SIMD4<Float>(p, 1)).xyz
                let distance = ray.interpolate(p)
                return (true, distance)
            }
        }
        return (false, nil)
    }
    
    func testData() {
        
//        let red = SIMD4<Float>(1, 0, 0, 1)
//        let orange = SIMD4<Float>(1, 165/255, 0, 1)
//        let yellow = SIMD4<Float>(1, 1, 0, 1)
//        let green = SIMD4<Float>(0, 1, 0, 1)
//        let cyan = SIMD4<Float>(0, 1, 1, 1)
//        let blue = SIMD4<Float>(0, 0, 1, 1)
//        let purple = SIMD4<Float>(128/255, 0, 128/255, 1)
//        let white = SIMD4<Float>(1, 1, 1, 1)
        //        let dlMesh = MDLMesh(hemisphereWithExtent: vector_float3(125, 125, 125), segments: vector_uint2(100, 100), inwardNormals: false, cap: true, geometryType: .triangles, allocator: bufferAllocator)
        // 1920 1080
        let segments = SIMD2<UInt16>(100, 100)
        let imageSize = SIMD2<Float>(1920, 1080)
        let scale = imageSize.x / (π * radius)
        let tcm = imageSize.y / scale / 2
        
        let radiansX = 2 * Float.pi / Float(segments.x)
        let radiansy = Float.pi / Float(segments.y)
        var vertex = Sphere.Vertex(position: SIMD3<Float>(0, radius, 0), color: SIMD4<Float>(0.5, 0, 0.5, 1), textureCoordinate: SIMD2<Float>(1,0))
        vertices.append(vertex)
        for i in 1..<segments.y {
            let y = cos(radiansy * Float(i)) * radius
            let r = sin(radiansy * Float(i)) * radius
            for j in 0..<segments.x {
                let radians = radiansX * Float(j)
                let x = sin(radians) * r
                let z = cos(radians) * r
                let color = SIMD4<Float>(sin(radians * 0.5), sin(radiansy * Float(i)), 1 - sin(radians * 0.5), 1)
                var textureCoordinate = SIMD2<Float>(0,0)
                // 正常图片的纹理位置计算
                if y <= tcm, y >= -tcm, radians <= π {
                    textureCoordinate.y = (tcm - y) / (tcm * 2)
                    textureCoordinate.x = radians / π
                }
                // 鱼眼图片的纹理位置计算
//                if y <= tcm, y >= -tcm, radians >= 0, radians <= π {
//                    let h = radius - sqrt(radius*radius - x*x)
//                    
//                    textureCoordinate.y = (tcm - powf(sin(radians), 0.5) * y) / (tcm * 2)
//                    if radians >= π*0.5 {
//                        textureCoordinate.x = (radius - x) / radius * 0.5 + 0.5
//                    } else {
//                        textureCoordinate.x = x / radius * 0.5
//                    }
//                }
                
                let vertex = Sphere.Vertex(position: SIMD3<Float>(x, y, z), color: color, textureCoordinate: textureCoordinate)
                vertices.append(vertex)
            }
            if i == 1 {
                for x in 1..<segments.x {
                    indices.append(contentsOf: [0, x, x + 1])
                }
                indices.append(contentsOf: [0, segments.x, 1])
            } else {
                let count = UInt16(vertices.count)
                let up = count - segments.x * 2
                let current = count - segments.x
                for x in 1..<segments.x {
                    indices.append(contentsOf: [up + x - 1, current + x, current + x - 1])
                    indices.append(contentsOf: [up + x, current + x, up + x - 1])
                }
                indices.append(contentsOf: [up + segments.x - 1, current, current + segments.x - 1])
                indices.append(contentsOf: [up, current, up + segments.x - 1])
            }
        }
        vertex = Sphere.Vertex(position: SIMD3<Float>(0, -radius, 0), color: SIMD4<Float>(1, 1, 0, 1), textureCoordinate: SIMD2<Float>(1,0))
        vertices.append(vertex)
        let count = UInt16(vertices.count)
        let current = count - 1
        let up = current - segments.x
        for x in 1..<segments.x {
            indices.append(contentsOf: [current, up + x - 1, up + x])
        }
        indices.append(contentsOf: [current, up + segments.x - 1, up])
        buildBuffers()
    }
}
/*
 
varying highp vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
uniform highp vec2 center;
uniform highp float radius;
uniform highp float aspectRatio;
uniform highp float refractiveIndex;
 
// uniform vec3 lightPosition;
const highp vec3 lightPosition = vec3(-0.5, 0.5, 1.0);
const highp vec3 ambientLightPosition = vec3(0.0, 0.0, 1.0);

void main() {
 
    highp vec2 textureCoordinateToUse = vec2(textureCoordinate.x, (textureCoordinate.y * aspectRatio + 0.5 - 0.5 * aspectRatio));
    highp float distanceFromCenter = distance(center, textureCoordinateToUse);
    lowp float checkForPresenceWithinSphere = step(distanceFromCenter, radius);
    distanceFromCenter = distanceFromCenter / radius;
    highp float normalizedDepth = radius * sqrt(1.0 - distanceFromCenter * distanceFromCenter);
    highp vec3 sphereNormal = normalize(vec3(textureCoordinateToUse - center, normalizedDepth));
    highp vec3 refractedVector = 2.0 * refract(vec3(0.0, 0.0, -1.0), sphereNormal, refractiveIndex);
    refractedVector.xy = -refractedVector.xy;
    highp vec3 finalSphereColor = texture2D(inputImageTexture, (refractedVector.xy + 1.0) * 0.5).rgb;
    // Grazing angle lighting
    highp float lightingIntensity = 2.5 * (1.0 - pow(clamp(dot(ambientLightPosition, sphereNormal), 0.0, 1.0), 0.25));
    finalSphereColor += lightingIntensity;
    // Specular lighting
    lightingIntensity  = clamp(dot(normalize(lightPosition), sphereNormal), 0.0, 1.0);
    lightingIntensity  = pow(lightingIntensity, 15.0);
    finalSphereColor += vec3(0.8, 0.8, 0.8) * lightingIntensity;
    gl_FragColor = vec4(finalSphereColor, 1.0) * checkForPresenceWithinSphere;
}
*/
