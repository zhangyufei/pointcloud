//
//  Node.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  2:11 PM.
//    
//  

import MetalKit

class Node {
    
    private static var ID: UInt = 0
    private static func increasing() -> UInt {
        ID += 1
        return ID
    }
    
    let id: UInt
    var name: String = String()
    let device: MTLDevice
    
    var position = SIMD3<Float>(repeating: 0) {
        didSet {
            updateModelMatrix()
        }
    }
    var rotation = SIMD3<Float>(repeating: 0){
        didSet {
            updateModelMatrix()
        }
    }
    var scale = SIMD3<Float>(repeating: 1){
        didSet {
            updateModelMatrix()
        }
    }
    func updateModelMatrix() {
        var matrix = matrix_float4x4(translation: position)
        matrix = matrix.rotated(angle: rotation)
        matrix = matrix.scaled(by: scale)
        _modelMatrix = matrix
    }
    
    private var _modelMatrix: matrix_float4x4 = matrix_identity_float4x4
    var modelMatrix: matrix_float4x4 {
        return _modelMatrix
    }
    
    var fillMode: MTLTriangleFillMode = .fill
    
    var children: [Node] = []
    init(mtlDevice: MTLDevice) {
        id = Node.increasing()
        device = mtlDevice
    }
    
    func add(childNode: Node) {
        children.append(childNode)
    }
    private var _uniforms: Uniforms = Uniforms()
    func render(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        
        _uniforms.viewProjectionMatrix = uniforms.viewProjectionMatrix
        _uniforms.modelMatrix = matrix_multiply(uniforms.modelMatrix, modelMatrix)
//      _uniforms.normalMatrix = _uniforms.modelMatrix.upperLeft
        if let renderable = self as? Primitive {
//            commandEncoder.pushDebugGroup("name")
            renderable.doRender(commandEncoder: commandEncoder, uniforms: _uniforms)
//            commandEncoder.popDebugGroup()
        }
        
        for child in children {
            child.render(commandEncoder: commandEncoder,
                         uniforms: _uniforms)
        }
    }
    var isUserInteractionEnabled = true
    /// 子类去实现
    func hitTestWithSegment(ray: Ray, transform: matrix_float4x4) -> Float? { return nil }
    
    func hitTest(ray: Ray, transform: matrix_float4x4) -> (Node, Float)? {
        
        let matrix = transform * modelMatrix
        let nearest = hitTestWithSegment(ray: ray, transform: matrix)
        
        var nearestChildHit: (Node, Float)?
        for item in children {
            if let childHit = item.hitTest(ray: ray, transform: matrix) {
                if let neat = nearestChildHit {
                    if neat.1 > childHit.1 {
                        nearestChildHit = childHit
                    }
                } else {
                    nearestChildHit = childHit
                }
            }
        }
        
        switch (nearest, nearestChildHit) {
        case (nil, nil):
            return nil
        case (nil, let near):
            return near
        case let (near, nil):
            return (self, near) as? (Node, Float)
        case let (near1, near2):
            if near1 ?? 1000000 < near2?.1 ?? 1000000 {
                return (self, near1) as? (Node, Float)
            } else {
                return near2
            }
        }
    }
}

extension Node: Equatable {
    static func == (lhs: Node, rhs: Node) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Node: Hashable {
    var hashValue: Int {
        return Int(id)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

