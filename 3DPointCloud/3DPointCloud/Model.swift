//
//  Model.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/2/8  3:27 PM.
//    
//  

import MetalKit

class Model: Primitive {

    let modelURL: URL
    var meshes: [Mesh]?
    
    var boundingBox: MDLAxisAlignedBoundingBox?
    
    init(device: MTLDevice, model: URL) {
        modelURL = model
        super.init(device: device)
        
        vertexDescriptor = Model.vertexDescriptor
        loadmodel()
        
        vertexFunctionName = "vertex_model1_shader"
        fragmentFunctionName = "fragment_texture_shader"
        
        if meshes?.first(where: { $0.submeshes.first(where: { $0.textures.baseColor != nil }) != nil }) == nil {
            fragmentFunctionName = "fragment_shader"
        }
    }
    
    private func loadmodel() {
        let bufferAllocator = MTKMeshBufferAllocator(device: device)
        let asset = MDLAsset(url: modelURL, vertexDescriptor: nil, bufferAllocator: bufferAllocator)
        let (mdlMeshes, mtkMeshes) = try! MTKMesh.newMeshes(asset: asset, device: device)
        boundingBox = mdlMeshes.first?.boundingBox
        meshes = zip(mdlMeshes, mtkMeshes).map {
          Mesh(mdlMesh: $0.0, mtkMesh: $0.1)
        }
        MTKMetalVertexDescriptorFromModelIO(mdlMeshes[0].vertexDescriptor).map { (descriptor) -> Void in
            vertexDescriptor = descriptor
        }
    }
    
    override func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        guard let meshes = meshes,
              meshes.count > 0,
              let pipelineState = pipelineState else { return }
        commandEncoder.setRenderPipelineState(pipelineState)
        
        withUnsafePointer(to: uniforms) { (p) in
            commandEncoder.setVertexBytes(UnsafeRawPointer(p),
                                          length: MemoryLayout<Uniforms>.stride,
                                          index: 1)
        }
        commandEncoder.setFragmentSamplerState(samplerState, index: 0)
        commandEncoder.setCullMode(.none)
        commandEncoder.setTriangleFillMode(fillMode)
        for mesh in meshes {
            if let buffer = mesh.mtkMesh.vertexBuffers.first {
                commandEncoder.setVertexBuffer(buffer.buffer, offset: buffer.offset, index: 0)
            }
            
//            for (index, vertexBuffer) in mesh.mtkMesh.vertexBuffers.enumerated() {
//                commandEncoder.setVertexBuffer(vertexBuffer.buffer, offset: 0, index: index)
//            }
            
            for submesh in mesh.submeshes {
                commandEncoder.setFragmentTexture(submesh.textures.baseColor, index: 0)
                let mtkSubmesh = submesh.mtkSubmesh
                commandEncoder.drawIndexedPrimitives(type: mtkSubmesh.primitiveType,
                                                     indexCount: mtkSubmesh.indexCount,
                                                     indexType: mtkSubmesh.indexType,
                                                     indexBuffer: mtkSubmesh.indexBuffer.buffer,
                                                     indexBufferOffset: mtkSubmesh.indexBuffer.offset)
            }
        }
    }
    
    override func hitTestWithSegment(ray: Ray, transform: matrix_float4x4) -> Float? {
        guard isUserInteractionEnabled, let box = boundingBox else { return nil }
        let localRay = transform.inverse * ray
        guard let t = testRayAABBIntersection(boundingBox: box, ray: localRay) else { return nil }
        
        var intersection = localRay.extrapolate(t)
        intersection = (transform * SIMD4<Float>(intersection, 1)).xyz
        let distance = ray.interpolate(intersection)
        return distance < 0 ? nil : distance

        
        return hitTest1(ray: ray, transform: transform)
        if let mesh = meshes?.first,
           let data = mesh.mdlMesh.vertexAttributeData(forAttributeNamed: MDLVertexAttributePosition),
           let submeshes = mesh.mdlMesh.submeshes as? [MDLSubmesh],
           let atts = mesh.mdlMesh.vertexDescriptor.attributes as? [MDLVertexAttribute],
           let vertexAttribute = atts.first(where: { $0.name == MDLVertexAttributePosition }),
           vertexAttribute.format == .float3 {
            
            let rayT = transform.inverse * ray
            func testIntersect(v0: SIMD3<Float>, v1: SIMD3<Float>, v2: SIMD3<Float>) -> Float? {
                let (intersect, k) = intersectTriangle(ray: rayT, v0: v0, v1: v1, v2: v2)
                if intersect, let t = k?.t {
                    var p = rayT.extrapolate(t)
                    p = (transform * SIMD4<Float>(p, 1)).xyz
                    let distance = ray.interpolate(p)
                    return distance
                }
                return nil
            }
            
            for item in submeshes {
                guard item.geometryType == .triangles else { continue }
                let pointer = item.indexBuffer.map().bytes
                let indexCount = item.indexCount
                
                switch item.indexType {
                case .uInt32:
                    for i in 0..<indexCount/3 {
                        let (v0, v1, v2) = loadTriangle(pointer: pointer, data: data, type: UInt32.self, i: i, offset: vertexAttribute.offset)
                        let distance = testIntersect(v0: v0, v1: v1, v2: v2)
                        if distance != nil { return distance }
                    }
                case .uInt16:
                    for i in 0..<indexCount/3 {
                        let (v0, v1, v2) = loadTriangle(pointer: pointer, data: data, type: UInt16.self, i: i, offset: vertexAttribute.offset)
                        let distance = testIntersect(v0: v0, v1: v1, v2: v2)
                        if distance != nil { return distance }
                    }
                case .uInt8:
                    for i in 0..<indexCount/3 {
                        let (v0, v1, v2) = loadTriangle(pointer: pointer, data: data, type: UInt8.self, i: i, offset: vertexAttribute.offset)
                        let distance = testIntersect(v0: v0, v1: v1, v2: v2)
                        if distance != nil { return distance }
                    }
                default: return nil
                }
            }
        }
        return nil
    }
    
    func hitTest1(ray: Ray, transform: matrix_float4x4) -> Float? {
        
        guard let mesh = meshes?.first,
              let buffer = mesh.mtkMesh.vertexBuffers.first,
              let submesh = mesh.mtkMesh.submeshes.first,
              let atts = mesh.mdlMesh.vertexDescriptor.attributes as? [MDLVertexAttribute],
              let vertexAttribute = atts.first(where: { $0.name == MDLVertexAttributePosition }),
              vertexAttribute.format == .float3,
              submesh.primitiveType == .triangle,
              submesh.indexType == .uint32 else { return nil }
        
        let rayT = transform.inverse * ray
        
        let triangleCount = submesh.indexCount / 3
        guard let device = MTLCreateSystemDefaultDevice(),
              let library = device.makeDefaultLibrary(),
              let vertexFunction = library.makeFunction(name: "intersectTriangle"),
              let commandQueue = device.makeCommandQueue(),
              let commandBuffer = commandQueue.makeCommandBuffer(),
              let commandEncoder = commandBuffer.makeComputeCommandEncoder(),
              let rayBuffer = device.makeBuffer(length:MemoryLayout<Ray>.stride,  options:MTLResourceOptions.storageModeShared),
              let distancesBuffer = device.makeBuffer(bytes: Array(repeating: Float(-1), count: triangleCount),
                                              length: MemoryLayout<Float>.stride * triangleCount,
                                              options: []) else { return nil }
        var pipeLineContains: MTLComputePipelineState
        do {
            pipeLineContains = try device.makeComputePipelineState(function: vertexFunction)
        } catch {
            return nil
        }
        
        commandEncoder.setComputePipelineState(pipeLineContains)
        commandEncoder.setBuffer(buffer.buffer, offset: 0, index: 0)
        commandEncoder.setBuffer(submesh.indexBuffer.buffer, offset: submesh.indexBuffer.offset, index: 1)
        
        withUnsafeBytes(of: rayT) { (p) in
            if let baseAddress = p.baseAddress {
                rayBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Ray>.stride)
                commandEncoder.setBuffer(rayBuffer, offset: 0, index: 2)
            }
        }
        
        commandEncoder.setBuffer(distancesBuffer, offset: 0, index: 3)
        
//        let a = pipeLineContains.maxTotalThreadsPerThreadgroup
//        let b = pipeLineContains.threadExecutionWidth
        let threadsPerThreadgroup = MTLSize(width: pipeLineContains.threadExecutionWidth,height: 1,depth: 1)
        
        let threadgroupsCount = MTLSize(width: triangleCount / threadsPerThreadgroup.width,
                                   height: 1,
                                   depth: 1)
        commandEncoder.dispatchThreadgroups(threadgroupsCount, threadsPerThreadgroup: threadsPerThreadgroup)
        commandEncoder.endEncoding()
        
        commandBuffer.commit()
        commandBuffer.waitUntilCompleted()
        
        let s = distancesBuffer.contents().bindMemory(to: Float.self, capacity: triangleCount)
        let maxDistance: Float = 100000
        var min: Float = maxDistance
        for t in 0..<triangleCount {
            if s[t] < -1 { print(s[t]) }
            if s[t] > 0, s[t] < min {
                min = s[t];
            }
        }
        
        guard min < maxDistance else { return nil }
        var p = rayT.extrapolate(min)
        p = (transform * SIMD4<Float>(p, 1)).xyz
        min = ray.interpolate(p)
        
        return min
    }
}

extension Model {

    func loadTriangle<T: FixedWidthInteger>(pointer: UnsafeMutableRawPointer,
                 data: MDLVertexAttributeData,
                 type: T.Type,
                 i: Int,
                 offset: Int,
                 capacity: Int = MemoryLayout<SIMD3<Float>>.stride) ->
    (SIMD3<Float>, SIMD3<Float>, SIMD3<Float>) {
        
        let stride = MemoryLayout<T>.stride
        let i0 = pointer.load(fromByteOffset: stride*(i*3), as: T.self)
        let i1 = pointer.load(fromByteOffset: stride*(i*3+1), as: T.self)
        let i2 = pointer.load(fromByteOffset: stride*(i*3+2), as: T.self)
        
        let v0 = (data.dataStart + Int(i0)*data.stride + offset).bindMemory(to: SIMD3<Float>.self, capacity: capacity).pointee
        let v1 = (data.dataStart + Int(i1)*data.stride + offset).bindMemory(to: SIMD3<Float>.self, capacity: capacity).pointee
        let v2 = (data.dataStart + Int(i2)*data.stride + offset).bindMemory(to: SIMD3<Float>.self, capacity: capacity).pointee
        return (v0, v1, v2)
    }
    
    func enumeratedVertexs() {
        
        if let mesh = meshes?.first,
           let data = mesh.mdlMesh.vertexAttributeData(forAttributeNamed: MDLVertexAttributePosition),
           let submeshes = mesh.mdlMesh.submeshes as? [MDLSubmesh] {
            
            let atts = mesh.mdlMesh.vertexDescriptor.attributes as? [MDLVertexAttribute]
            guard let vertexAttribute = atts?.first(where: { $0.name == MDLVertexAttributePosition }), vertexAttribute.format == .float3 else { return }
            
            for item in submeshes {
                guard item.geometryType == .triangles else { continue }
                let pointer = item.indexBuffer.map().bytes
                let indexCount = item.indexCount
                
                switch item.indexType {
                case .uInt32:
                    for i in 0..<indexCount/3 {
                        let (v0, v1, v2) = loadTriangle(pointer: pointer, data: data, type: UInt32.self, i: i, offset: vertexAttribute.offset)
                        print(v0, v1, v2)
                    }
                case .uInt16:
                    for i in 0..<indexCount/3 {
                        let triangle = loadTriangle(pointer: pointer, data: data, type: UInt16.self, i: i, offset: vertexAttribute.offset)
                    }
                case .uInt8:
                    for i in 0..<indexCount/3 {
                        let triangle = loadTriangle(pointer: pointer, data: data, type: UInt8.self, i: i, offset: vertexAttribute.offset)
                    }
                default: return
                }
                
            }
            
        }
        
        let uint32Stride = MemoryLayout<UInt32>.stride
        if let mesh = meshes?.first, let buffer = mesh.mtkMesh.vertexBuffers.first?.buffer.contents() {
            let stride = vertexDescriptor.layouts[0].stride
            let off = mesh.mtkMesh.vertexBuffers[0].offset
            
            let atts = mesh.mdlMesh.vertexDescriptor.attributes as? [MDLVertexAttribute]
            guard let vertexAttribute = atts?.first(where: { $0.name == MDLVertexAttributePosition }), vertexAttribute.format == .float3 else { return }
            
            for submesh in mesh.submeshes {
                let offset = submesh.mtkSubmesh.indexBuffer.offset
                let count = submesh.mtkSubmesh.indexCount
                let pointer = submesh.mtkSubmesh.indexBuffer.buffer.contents()
                
                switch submesh.mtkSubmesh.indexType {
                case .uint16:
                    break
                case .uint32:
                    for i in 0..<count/3 {
                        let i0 = pointer.load(fromByteOffset: offset + uint32Stride*(i*3), as: UInt32.self)
                        let i1 = pointer.load(fromByteOffset: offset + uint32Stride*(i*3+1), as: UInt32.self)
                        let i2 = pointer.load(fromByteOffset: offset + uint32Stride*(i*3+2), as: UInt32.self)
                        
                        let v0 = buffer.load(fromByteOffset: off + Int(i0)*stride + vertexAttribute.offset, as: SIMD3<Float>.self)
                        let v1 = buffer.load(fromByteOffset: off + (Int(i1)+1)*stride + vertexAttribute.offset, as: SIMD3<Float>.self)
                        let v2 = buffer.load(fromByteOffset: off + (Int(i2)+2)*stride + vertexAttribute.offset, as: SIMD3<Float>.self)
                        print(v0, v1, v2)
                    }
                @unknown default:
                    break
                }
            }
        }
    }
    
    static var vertexDescriptor: MTLVertexDescriptor = {
        let floatStride = MemoryLayout<Float>.stride
        let vertexDescriptor = MTLVertexDescriptor()
        
        vertexDescriptor.attributes[0].format = .float3
        vertexDescriptor.attributes[0].offset = 0
        vertexDescriptor.attributes[0].bufferIndex = 0
        
        vertexDescriptor.attributes[1].format = .float4
        vertexDescriptor.attributes[1].offset = floatStride * 3
        vertexDescriptor.attributes[1].bufferIndex = 0
        
        vertexDescriptor.attributes[2].format = .float2
        vertexDescriptor.attributes[2].offset = floatStride * 7
        vertexDescriptor.attributes[2].bufferIndex = 0
        
        vertexDescriptor.attributes[3].format = .float3
        vertexDescriptor.attributes[3].offset = floatStride * 9
        vertexDescriptor.attributes[3].bufferIndex = 0
        
        vertexDescriptor.layouts[0].stride = floatStride * 12
        
        return vertexDescriptor
    }()
}
