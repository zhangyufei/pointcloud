//
//  Points.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  3:59 PM.
//    
//  

import MetalKit

class Points: Primitive {
    
    let filePath: URL?
    
    init(device: MTLDevice, path: URL?) {
        filePath = path
        super.init(device: device)
    }
    
    init(device: MTLDevice, vertices: [Vertex]) {
        filePath = nil
        super.init(device: device)
        self.vertices = vertices
        buildBuffers()
    }
    
    override func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        guard let pipelineState = pipelineState  else { return }
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setVertexBuffer(vertexBuffer,
                                       offset: 0, index: 0)
        withUnsafePointer(to: uniforms) { (p) in
            commandEncoder.setVertexBytes(UnsafeRawPointer(p),
                                          length: MemoryLayout<Uniforms>.stride,
                                          index: 1)
        }
        commandEncoder.drawPrimitives(type: .point, vertexStart: 0, vertexCount: vertices.count)
    }
}

extension Points {
    func calcVertices(cube: Cube) -> [Vertex] {
        let box = Box(cube: cube)
        let vertices = self.vertices as! [Vertex]
        guard let vertexBuffer = vertexBuffer,
              let library = device.makeDefaultLibrary(),
              let vertexFunction = library.makeFunction(name: "vertices_contains_hide"),
              let commandQueue = device.makeCommandQueue(),
              let commandBuffer = commandQueue.makeCommandBuffer(),
              let commandEncoder = commandBuffer.makeComputeCommandEncoder(),
              let boundingBoxBuffer = device.makeBuffer(length:MemoryLayout<Box>.stride,  options:MTLResourceOptions.storageModeShared) else { return [] }
        var pipeLineContains: MTLComputePipelineState
        do {
            pipeLineContains = try device.makeComputePipelineState(function: vertexFunction)
        } catch {
            return []
        }
        
        commandEncoder.setComputePipelineState(pipeLineContains)
        commandEncoder.setBuffer(vertexBuffer, offset: 0, index: 0)
        withUnsafeBytes(of: box) { (p) in
            if let baseAddress = p.baseAddress {
                boundingBoxBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Box>.stride)
                commandEncoder.setBuffer(boundingBoxBuffer, offset: 0, index: 1)
            }
        }
        
//        withUnsafeBytes(of: matrix) { (p) in
//            if let baseAddress = p.baseAddress {
//                matrixBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<matrix_float4x4>.stride)
//                commandEncoder.setBuffer(matrixBuffer, offset: 0, index: 2)
//            }
//        }
//        var indexes: [Int32] = Array(repeating: -1, count: vertices.count)
//        withUnsafeBytes(of: indexes) { (p) in
//            if let baseAddress = p.baseAddress {
//                indexesBuffer.contents().copyMemory(from: baseAddress, byteCount: MemoryLayout<Int32>.stride * vertices.count)
//                commandEncoder.setBuffer(indexesBuffer, offset: 0, index: 3)
//            }
//        }
//        let a = pipeLineContains.maxTotalThreadsPerThreadgroup
//        let b = pipeLineContains.threadExecutionWidth
        let threadsPerThreadgroup = MTLSize(width: pipeLineContains.threadExecutionWidth,height: 1,depth: 1)
        
        let threadgroupsCount = MTLSize(width: Int(ceil(Float(vertices.count) / Float(threadsPerThreadgroup.width))),
                                   height: 1,
                                   depth: 1)
        commandEncoder.dispatchThreadgroups(threadgroupsCount, threadsPerThreadgroup: threadsPerThreadgroup)
        commandEncoder.endEncoding()
        
        commandBuffer.commit()
        commandBuffer.waitUntilCompleted()
        
        let arr = vertexBuffer.contents().bindMemory(to: Vertex.self, capacity: vertices.count)
        var verticesArr = [Vertex]()
        for i in 0..<vertices.count {
            if arr[i].type == 1 {
                verticesArr.append(arr[i])
            }
        }
        return verticesArr
    }
    
    struct Box {
        let maxBounds: SIMD3<Float>
        let minBounds: SIMD3<Float>
        let matrix: matrix_float4x4
        
        init(cube: Cube) {
            maxBounds = cube.boundingBox.maxBounds
            minBounds = cube.boundingBox.minBounds
            matrix = cube.modelMatrix.inverse
        }
    }
    

    func calcVertices(cubes: [Cube], type: Int32 = 1) {
        guard cubes.count > 0 else { return }
        
        let boxes = cubes.map { Box(cube: $0) }
        let count = boxes.count
        let vertices = self.vertices as! [Vertex]
        guard let vertexBuffer = vertexBuffer,
              let library = device.makeDefaultLibrary(),
              let vertexFunction = library.makeFunction(name: "vertices_contains_change_type"),
              let commandQueue = device.makeCommandQueue(),
              let commandBuffer = commandQueue.makeCommandBuffer(),
              let commandEncoder = commandBuffer.makeComputeCommandEncoder(),
              let boxBuffer = device.makeBuffer(length:MemoryLayout<Box>.stride * count,  options:MTLResourceOptions.storageModeShared),
              let countBuffer = device.makeBuffer(length:MemoryLayout<Int32>.stride,  options:MTLResourceOptions.storageModeShared),
              let typeBuffer = device.makeBuffer(length:MemoryLayout<Int32>.stride,  options:MTLResourceOptions.storageModeShared) else { return }
        var pipeLineContains: MTLComputePipelineState
        do {
            pipeLineContains = try device.makeComputePipelineState(function: vertexFunction)
        } catch {
            return
        }
        
        commandEncoder.setComputePipelineState(pipeLineContains)
        commandEncoder.setBuffer(vertexBuffer, offset: 0, index: 0)
        
        boxes.withUnsafeBufferPointer { (p) in
            if let baseAddress = p.baseAddress {
                boxBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Float>.stride * count)
                boxBuffer.contents().initializeMemory(as: Box.self, from: baseAddress, count: p.count)
                commandEncoder.setBuffer(boxBuffer, offset: 0, index: 1)
            }
        }
        
        let count32 = Int32(count)
        withUnsafeBytes(of: count32) { (p) in
            if let baseAddress = p.baseAddress {
                countBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Int32>.stride)
                commandEncoder.setBuffer(countBuffer, offset: 0, index: 2)
            }
        }
        
        withUnsafeBytes(of: type) { (p) in
            if let baseAddress = p.baseAddress {
                typeBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Int32>.stride)
                commandEncoder.setBuffer(typeBuffer, offset: 0, index: 3)
            }
        }
        
        let threadsPerThreadgroup = MTLSize(width: pipeLineContains.threadExecutionWidth,height: 1,depth: 1)
        
        let threadgroupsCount = MTLSize(width: Int(ceil(Float(vertices.count) / Float(threadsPerThreadgroup.width))),
                                   height: 1,
                                   depth: 1)
        commandEncoder.dispatchThreadgroups(threadgroupsCount, threadsPerThreadgroup: threadsPerThreadgroup)
        commandEncoder.endEncoding()
        
        commandBuffer.commit()
        commandBuffer.waitUntilCompleted()
        
        let arr1 = vertexBuffer.contents().bindMemory(to: Vertex.self, capacity: vertices.count)
        var verticesArr = [Vertex]()
        for i in 0..<vertices.count {
            if arr1[i].type >= 1 {
                verticesArr.append(arr1[i])
            }
        }
        print(verticesArr.count)
        
    }

}

extension Points {
    
    func copy() -> Points {
        let p = Points(device: device, path: filePath)
        p.vertices = vertices
        p.indices = indices
        p.buildBuffers()
        return p
    }
    
    func testData() {
        guard let filePath = filePath,
              let vs = Points.loadData(filePath) else { return }
        
//        var vs = [Primitive.Vertex]()
//        var color = SIMD4<Float>(0,1,1,1)
//        for y in -10...10 {
//            for x in -10...10 {
//
//                let position = SIMD3<Float>(Float(x * 10), Float(y * 10), 0)
//                let vertex = Primitive.Vertex(position: position, color: color)
//                vs.append(vertex)
//            }
//        }
        
        vertices = vs
        buildBuffers()
    }
    /*
     0
     VERSION 0.7                    1
     FIELDS x y z intensity label   2
     SIZE 4 4 4 4 4                 3
     TYPE F F F F U                 4
     COUNT 1 1 1 1 1                5
     WIDTH 82084                    6
     HEIGHT 1                       7
     VIEWPOINT 0 0 0 1 0 0 0        8
     POINTS 82084                   9
     DATA ascii                     10
     */
    static func loadData(_ filePath: URL) -> [Primitive.Vertex]? {
        guard let data = try? String(contentsOf: filePath, encoding: .ascii) else { return nil }
        
        let lines = data.components(separatedBy: .newlines)
        var vertexs = [Primitive.Vertex]()
        var color = SIMD4<Float>(0,1,1,1)
        
        var xMin: Float = 0, yMin: Float = 0, zMin: Float = 0, iMin: Float = 0, lMin: UInt = 0
        var xMax: Float = 0, yMax: Float = 0, zMax: Float = 0, iMax: Float = 0, lMax: UInt = 0
        for item in lines[11..<lines.count] {
            let fs = item.components(separatedBy: .whitespaces)
            if let x = Float(fs[0]),
               let y = Float(fs[1]),
               let z = Float(fs[2]),
               let intensity = Float(fs[3]),
               let label = UInt(fs[4]) {
                
                if x > xMax { xMax = x }
                if y > yMax { yMax = y }
                if z > zMax { zMax = z }
                if intensity > iMax { iMax = intensity }
                if label > lMax { lMax = label }
                
                if x < xMin { xMin = x }
                if y < yMin { yMin = y }
                if z < zMin { zMin = z }
                if intensity < iMin { iMin = intensity }
                if label < lMin { lMin = label }
                
                let position = SIMD3<Float>(x, y, z)
//                let v = (x+375)/(750/7)
//                switch v {
//                case ..<1:
//                    color = SIMD4<Float>(v,0,0,1)
//                case 1..<2:
//                    color = SIMD4<Float>(1,(v-1)*128/255,0,1)
//                case 2..<3:
//                    color = SIMD4<Float>(1,(v-2),0,1)
//                case 3..<4:
//                    color = SIMD4<Float>(0,(v-3),0,1)
//                case 4..<5:
//                    color = SIMD4<Float>(0,(v-4),(v-4),1)
//                case 5..<6:
//                    color = SIMD4<Float>(0,0,(v-5),1)
//                default:
//                    color = SIMD4<Float>((v-6)*0.5,0,v-6,1)
//                }
                switch z {
                case ..<0:
                    color = SIMD4<Float>(117/255.0,56/255.0,117/255.0,1)
                case 0..<2.3:
                    color = SIMD4<Float>(0,1,0,1)
                default:
                    color = SIMD4<Float>(96/255.0,131/255.0,32/255.0,1)
                }
                switch (x, y) {
                case (-1.8..<1.8, -1..<1):
                    color = SIMD4<Float>(0,0,1,1)
                default:
                    break
                }
                let vertex = Primitive.Vertex(position: position, color: color)
                vertexs.append(vertex)
            }
        }
        
//        print(xMin, yMin, zMin, iMin, lMin)
//        print(xMax, yMax, zMax, iMax, lMax)
        
        let count = lines[9]
        if count.contains("POINTS") {
            let cs = count.components(separatedBy: .whitespaces)[1]
            if let c = Int(cs) {
                if c == vertexs.count {
                    print("数据解析完成")
                    return vertexs
                }
            }
        }
        return nil
    }
}
