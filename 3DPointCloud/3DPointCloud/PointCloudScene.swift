//
//  PointCloudScene.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  3:55 PM.
//    
//  

import MetalKit

class PointCloudScene: Scene {
    
    var points: Points
    let cube: Cube
    
    struct Transform {
        var scale = CGFloat(1)
    }
    /// 记录手势的起始状态
    var tmpTransform = Transform()
    
    lazy var borderLayer: CALayer = {
        let borderLayer = CALayer()
        borderLayer.backgroundColor = UIColor.red.withAlphaComponent(0.3).cgColor
        borderLayer.borderWidth = 1
        borderLayer.borderColor = UIColor.white.cgColor
        return borderLayer
    }()
    
    let bezierPath = UIBezierPath()
    lazy var shapeLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = UIColor(red: 235/255.0, green: 255/255.0, blue: 0/255.0, alpha: 0.3).cgColor
        layer.lineCap = .round
        layer.lineJoin = .round
        layer.lineWidth = 26
        return layer
    }()
    
    init?(device: MTLDevice, size: CGSize, view: MTKView, fileName: String) {
        
        guard let path = Bundle.main.url(forResource: fileName, withExtension: "pcd") else { return nil }
        let ps = Points(device: device, path: path)
        ps.testData()
        points = ps
        
        let normal = Lines(device: device)
        normal.dataNormal()
        points.add(childNode: normal)

        let border = Lines(device: device)
        border.testData()
        points.add(childNode: border)
        
        if let textureURL = Bundle.main.url(forResource: "1604131731122935", withExtension: "jpg") {
            let texture = Texture(device: device, texture: textureURL)
            texture.testData()
            texture.position += [120, 0, -1]
            texture.rotation += [0, -Float.pi * 0.5, -Float.pi * 0.5]
            points.add(childNode: texture)
        }

        if let textureURL = Bundle.main.url(forResource: "1604131731118812", withExtension: "jpg") {
            let texture = Texture(device: device, texture: textureURL)
            texture.testData()
            texture.position += [-120, 0, -1]
            texture.rotation += [0, Float.pi * 0.5, Float.pi * 0.5]
            points.add(childNode: texture)
        }
//        let plane = Plane(device: device, view: view)
//        plane.testData()
//        plane.position.x = 250
//        plane.rotation.x = -Float.pi * 0.5
//        plane.rotation.y = Float.pi * 0.5
//        points.add(childNode: plane)
        
        cube = Cube(device: device)
        cube.testData()
//        points.add(childNode: cube)
        
        super.init(device: device, size: size, view: view)
        
        add(childNode: points)
        setGestureRecognizer()
        
//        if let mu = Bundle.main.url(forResource: "cubeImage", withExtension: "obj") {
//            let people = Model(device: device, view: view, model: mu)
//            people.position.z = 1
//
////            people.scale = simd_float3(repeating: 0.3)
//            people.rotation.z = Float.pi * 0.25
//
//            points.add(childNode: people)
//        }
        
//        MetalCalcVertices().test_func()
        
//        let sphere = Sphere(device: device, view: view, radius: 125)
//        sphere.testData()
//        sphere.rotation.z = Float.pi * 0.5
//        sphere.rotation.y = -Float.pi * 0.5
//        points.add(childNode: sphere)
        
//        let sphere1 = Sphere(device: device, view: view, radius: 10)
//        sphere1.testData()
//        sphere1.position.x = -20
//        sphere.rotation.z = Float.pi * 0.5
//        sphere.rotation.y = -Float.pi * 0.5
//        points.add(childNode: sphere1)
//        translation()
        
        
        view.superview?.addSubview(segmentedControl)
    }
    
    lazy var segmentedControl: WBSegmented = {
        
        var frame = CGRect(x: size.width - 61 - 16, y: size.height - 64, width: 61, height: 32)
        frame = view.convert(frame, to: view.superview)
        let control = WBSegmented(frame: frame)
        control.handlebuttonClick = { [weak self] (isLeft: Bool) in
            if isLeft {
                self?.shapeLayer.strokeColor = UIColor(red: 235/255.0, green: 255/255.0, blue: 0/255.0, alpha: 0.3).cgColor
            } else {
                self?.shapeLayer.strokeColor = UIColor(red: 255/255.0, green: 0/255.0, blue: 255/255.0, alpha: 0.5).cgColor
            }
        }
        return control
    }()
    
    func touchEnd() {
        if let cubes = testIntersection() {
            let type: Int32 = segmentedControl.isSelectedLeft ? 1 : 0
            points.calcVertices(cubes: cubes, type: type)
        }
        shapeLayer.removeFromSuperlayer()
        shapeLayer.path = nil
        bezierPath.removeAllPoints()
        lines.removeAll()
    }
    
    func testIntersection() -> [Cube]? {
        
        guard let cubes = (points.children.filter { (node) -> Bool in
            guard let c = node as? Cube else { return false }
            if segmentedControl.isSelectedLeft {
                return c.fillMode == .fill
            } else {
                return c.fillMode == .lines
            }
        } as? [Cube]), cubes.count > 0, lines.count > 1 else { return nil }
        var selection = [Cube]()
        let modelMatrix = camera.viewMatrix * self.modelMatrix * points.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        let positions = lines.map { viewSpaceToHorizontalPlane($0.vector2(), modelMatrix, camera.projectionMatrix, viewPort)!.xy }
        let matrixRotate = matrix_float2x2([0, 1], [-1,0])
        let scale = viewSpaceToWorldScale(Float(shapeLayer.lineWidth * 0.5), modelMatrix, camera.projectionMatrix, viewPort) ?? 1
        
        if cubes.count > 20 {
            return calcVertices(cubes: cubes, line: positions, scale: scale)
        }
        
        for cube in cubes {
            let cubeModelMatrix = cube.modelMatrix
            let minbounds = (cubeModelMatrix * vector4(cube.boundingBox.minBounds, 1)).xy
            let maxBounds = (cubeModelMatrix * vector4(cube.boundingBox.maxBounds, 1)).xy
            
            var start = positions[0]
            
            for end in positions[1..<lines.count] {
                var result = testLineSegmentIntersection(start: start, end: end, minBounds: minbounds, maxBounds: maxBounds)
                
                func changeCubeState(cube: Cube) {
                    if segmentedControl.isSelectedLeft  {
                        cube.fillMode = .lines
                        cube.color = SIMD4<Float>(1, 0, 0, 1)
                    } else {
                        cube.fillMode = .fill
                        cube.color = SIMD4<Float>(1, 1, 1, 1)
                    }
                    cube.buildVertices()
                    selection.append(cube)
                }
                
                if result {
                    changeCubeState(cube: cube)
                    break
                }
                
                let translation = normalize(matrixRotate * (end - start)) * scale
                
                var start1 = start + translation
                var end1 = end + translation
                result = testLineSegmentIntersection(start: start1, end: end1, minBounds: minbounds, maxBounds: maxBounds)
                if result {
                    changeCubeState(cube: cube)
                    break
                }
                
                start1 = start - translation
                end1 = end - translation
                result = testLineSegmentIntersection(start: start1, end: end1, minBounds: minbounds, maxBounds: maxBounds)
                if result {
                    changeCubeState(cube: cube)
                    break
                }
                
                start = end
            }
        }
        
        if l == nil {
            l = Lines(device: device)
        }
        l!.dataLines(line: [positions])
        points.add(childNode: l!)
        
        return selection
    }
    var l: Lines?
    func translation() {
        
        let center = vector2(Float(size.width * 0.5), Float(size.height - 10))
        let modelMatrix = camera.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        guard let result = viewSpaceToHorizontalPlane(center, modelMatrix, camera.projectionMatrix, viewPort) else { return }
        
        (camera as? ArcballCamera)?.setTarget(position: -result)
    }
    
    var location: CGPoint?
    func boxlabeling(start: CGPoint, end: CGPoint) {
        let statVector3 = vector2(Float(start.x), Float(start.y))
        let endVector3 = vector2(Float(end.x), Float(end.y))
        
        let modelMatrix = camera.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        let result = viewSpaceToHorizontalPlane(statVector3, modelMatrix, camera.projectionMatrix, viewPort)
        let result1 = viewSpaceToHorizontalPlane(endVector3, modelMatrix, camera.projectionMatrix, viewPort)
        
        if let r1 = result, let r2 = result1 {
            
            print(r1)
            print(r2)
            
        }
    }
    
    func transPoint(p: CGPoint) -> vector_float3? {
        let vector = vector2(Float(p.x), Float(p.y))
        
        let modelMatrix = camera.viewMatrix * self.modelMatrix * points.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        return viewSpaceToHorizontalPlane(vector, modelMatrix, camera.projectionMatrix, viewPort)
    }
    
    func displayRay(p: CGPoint) -> (near: simd_float3, far: simd_float3)? {
        
        let vector = vector2(Float(p.x), Float(p.y))
        
        let modelMatrix = camera.viewMatrix * self.modelMatrix * points.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        guard let (near, far) = screenPosToWorldRay(vector, modelMatrix, camera.projectionMatrix, viewPort) else { return nil }
        return (near, far)
    }
    var mark: UInt = 0
    func addline(near: simd_float3, far: simd_float3) {
        let lines = (points.children.first { $0 is Lines && $0.id == mark }) as? Lines ?? Lines(device: device)
        lines.dataRay(near: near, far: far)
        if mark == 0 {
            points.add(childNode: lines)
        }
        mark = lines.id
    }
    
    var selectNode: Node?
    func selectDetection(p: CGPoint) {
        let vector = vector2(Float(p.x), Float(p.y))
        
        let modelMatrix = camera.viewMatrix * self.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        guard let (near, far) = screenPosToWorldRay(vector, modelMatrix, camera.projectionMatrix, viewPort) else { return }
        let direction = simd.normalize(far - near)
        let ray = Ray(origin: near, direction: direction)
        
        guard let result = points.hitTest(ray: ray, transform: matrix_identity_float4x4) else { return }
        
        if let s = selectNode {
            selectNode = nil
            s.fillMode = .fill
            if let cube = s as? Cube {
                cube.color = SIMD4<Float>(1, 1, 1, 1)
                cube.buildVertices()
            }
            
            if s == result.0 {
                selectNode = nil
                return
            }
        }
        
        selectNode = result.0
        
        if result.0.fillMode == .fill {
            result.0.fillMode = .lines
            if let cube = result.0 as? Cube {
                cube.color = SIMD4<Float>(1, 0, 0, 1)
                cube.buildVertices()
            }
        }
    }
    
    func addBorder(point: CGPoint) {
        
        let vector = vector2(Float(point.x), Float(point.y))
        let modelMatrix = camera.viewMatrix * self.modelMatrix * points.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        guard let position = viewSpaceToHorizontalPlane(vector, modelMatrix, camera.projectionMatrix, viewPort) else { return }
        let cube = border
        cube.frame = Cube.Position(origin: [-1, -1, -1], size: [2,2,2])
        cube.position = position
        cube.buildVertices()
        points.add(childNode: cube)
    }
    
    func addBorder(near: CGPoint, far: CGPoint) {
        
        let nearVector = vector2(Float(near.x), Float(near.y))
        let farVector = vector2(Float(far.x), Float(far.y))
        let modelMatrix = camera.viewMatrix * self.modelMatrix * points.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        guard var position1 = viewSpaceToHorizontalPlane(nearVector, modelMatrix, camera.projectionMatrix, viewPort),
              var position2 = viewSpaceToHorizontalPlane(farVector, modelMatrix, camera.projectionMatrix, viewPort) else { return }
        var position = (position1 + position2) * 0.5
        position.z = 1.5
        
        let matrixRotateZ = (camera as! ArcballCamera).matrixRotateZ
        position1 = (matrixRotateZ.inverse * vector4(position1, 1)).xyz
        position2 = (matrixRotateZ.inverse * vector4(position2, 1)).xyz
        let dis = abs(position1 - position2) * 0.5
        let cube = border
        
        cube.frame = Cube.Position(origin: [-dis.x, -dis.y, -1.7], size: [dis.x*2,dis.y*2,3])
        cube.position = position
        cube.rotation.z = (camera as! ArcballCamera).rotationZ
        cube.buildVertices()
        points.add(childNode: cube)
    }
    
    /// 开始移动之前的信息
    private var package: (beganPoint: CGPoint, activeDotPosition: CGRect.Position, beginRect: CGRect) = (CGPoint.zero, .leftTop(CGPoint.zero), CGRect.zero)
    var renderers: [Renderer] = []
    func addThreeView(_ cube: Cube) {
        
        let vertices = points.calcVertices(cube: cube)
        guard vertices.count > 0 else { return }
//        if let boundingBox = vertices.boundingBox(cube.modelMatrix.inverse) {
//            cube.frame.transform(boundingBox: boundingBox)
//            cube.buildVertices()
//        }
        let points = Points(device: device, vertices: vertices)
        
        for i in 0..<3 {
            
            let rect: CGRect
            if i == 0 {
                rect = CGRect(x: 0, y: 50, width: 414, height: 200)
            } else if i == 1 {
                rect = CGRect(x: 0, y: 896 - 400 - 34, width: 414, height: 200)
            } else {
                rect = CGRect(x: 0, y: 896 - 200 - 34, width: 414, height: 200)
            }
            guard let (mtlView, renderer) = mtlView(frame: rect) else { return }
            
            let scene = PartPointScene(device: device, size: mtlView.frame.size, view: mtlView, points: points, cube: cube)
            if let camera = scene.camera as? ArcballCamera {
                camera.setTarget(position: cube.position)
                
                let rotation: Float, angle: Float
                if i == 0 {
                    rotation = cube.rotation.z
                    angle = 0
                    scene.viewType = .overlook
                } else if i == 1 {
                    rotation = cube.rotation.z
                    angle = Float.pi * 0.5
                    scene.viewType = .front
                } else {
                    rotation = cube.rotation.z + Float.pi * 0.5
                    angle = Float.pi * 0.5
                    scene.viewType = .side
                }
                camera.setRotationZ(rotation: rotation)
                camera.setDepressionAngle(angle: angle)
                
                if camera.isProjection {
                    camera.setDistance(distance: 6)
                } else {
                    camera.viewPortWidth = cube.frame.size.max() * 0.6
                }
            }
            
            scene.displayBorder()
            
            renderer.scene = scene
            renderers.append(renderer)
            view.addSubview(mtlView)
        }
        
    }
    
    func mtlView(frame: CGRect) -> (MTKView, Renderer)? {
        let mtlView = MTKView(frame: frame)
        mtlView.colorPixelFormat = .bgra8Unorm
        mtlView.layer.borderWidth = 2
        mtlView.layer.borderColor = UIColor.white.cgColor
        guard let renderer = Renderer(view: mtlView) else { return nil }
        return (mtlView, renderer)
    }
    
    override func setGestureRecognizer() {
        super.setGestureRecognizer()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tap2.numberOfTapsRequired = 2
        view.addGestureRecognizer(tap2)
        tap.require(toFail: tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tap3.numberOfTapsRequired = 3
        view.addGestureRecognizer(tap3)
        tap2.require(toFail: tap3)
        
//        pinch.require(toFail: rotation)
    }
    /// X和Y轴的旋转
    @objc override func handlePanGestureRecognizer(_ pan: UIPanGestureRecognizer) {
        
        if pan.numberOfTouches == 2 {
            super.handlePanGestureRecognizer(pan)
        } else {
            drawShapeLayer(pan)
        }
        return
        let translation = pan.translation(in: view)
        if selectNode != nil {
            handleNodeRotate(translation)
            pan.setTranslation(.zero, in: pan.view)
        } else {
            super.handlePanGestureRecognizer(pan)
        }
    }
    
    var begin: CGPoint = CGPoint.zero
    var current: CGPoint = CGPoint.zero
    var lines = [CGPoint]()
    
    func drawShapeLayer(_ pan: UIPanGestureRecognizer) {
        let translation = pan.translation(in: view)
        switch pan.state {
        case .began:
            let location = pan.location(in: view)
            if shapeLayer.superlayer == nil {
                view.layer.addSublayer(shapeLayer)
            }
            begin = location
            lines.append(location)
            bezierPath.move(to: location)
        case .changed:
            current = begin + translation
            if let last = lines.last, last.distance(current) > 5 {
                lines.append(current)
                bezierPath.addLine(to: current)
                shapeLayer.path = bezierPath.cgPath
            }
        case .ended:
            touchEnd()
        default:
            break
        }
    }
    
    /// 处理长按手势
    @objc override func handleLongPressGestureRecognizer(_ press: UILongPressGestureRecognizer) {
        guard selectNode == nil else {
            press.state = .failed
            return
        }
        if press.numberOfTouchesRequired == 1 {
            let p = press.location(in: view)

            switch press.state {
            case .began:
                if borderLayer.superlayer == nil {
                    view.layer.addSublayer(borderLayer)
                    borderLayer.frame = CGRect(origin: p, size: CGSize.zero)
                    package.activeDotPosition = .rightBottom(p)
                } else {
                    let frame = borderLayer.frame
                    package.activeDotPosition = frame.minDistance(point: p)
                }
                borderLayer.backgroundColor = UIColor.clear.cgColor
                package.beginRect = borderLayer.frame
                package.beganPoint = p
            case .changed:
                
                borderLayer.frame = package.beginRect.changeFame(p - package.beganPoint, position: package.activeDotPosition)
//            case .ended:
//                break
            default:
                borderLayer.backgroundColor = UIColor.red.withAlphaComponent(0.3).cgColor
                break
            }
        }
    }
}

extension PointCloudScene {
    
    func handleNodeRotate(_ translation: CGPoint) {
        guard let node = selectNode else { return }
        node.rotation.z += Float(translation.x * 0.01)
    }
    
    /// 点击处理
    @objc func handleTapGestureRecognizer(_ tap: UITapGestureRecognizer) {
        
        if tap.numberOfTapsRequired == 1 {
            let p = tap.location(in: view)
            if let (near, far) = displayRay(p: p) {
                addline(near: near, far: far)
                selectDetection(p: p)
            }
        } else if tap.numberOfTapsRequired == 2 {
            if borderLayer.superlayer != nil {
                borderLayer.removeFromSuperlayer()
                let frame = borderLayer.frame
                var p1 = frame.origin
                var p2 = CGPoint(x: frame.origin.x + frame.size.width, y: frame.origin.y + frame.size.height)
                if p1.x > p2.x { swap(&p1.x, &p2.x)}
                if p1.y > p2.y { swap(&p1.y, &p2.y)}
                addBorder(near: p1, far: p2)
            } else if let node = selectNode, let cube = node as? Cube {
                selectNode = nil
                node.fillMode = .fill
                if let cube = node as? Cube {
                    cube.color = SIMD4<Float>(1, 1, 1, 1)
                    cube.buildVertices()
                    cube.isUserInteractionEnabled = false
                }
                
                addThreeView(cube)
            }
            
        } else if tap.numberOfTapsRequired == 3 {
            if let node = selectNode, let index = points.children.firstIndex(where: { $0 == node }) {
                selectNode = nil
                points.children.remove(at: index)
            }
        }
    }
}

extension PointCloudScene {
    
    var border: Cube {
        let cube = Cube(device: device)
        cube.name = "cube border"
        return cube
    }
}

extension PointCloudScene {
    
    func calcPointsVertices() {
        let position = cube.frame
        let x = position.origin.x;
        let y = position.origin.y;
        let z = position.origin.z;
        let maxX = position.origin.x + position.size.x;
        let maxY = position.origin.y + position.size.y;
        let maxZ = position.origin.z + position.size.z;
        
        
        var xx: Float = 120, yy: Float = 120, zz: Float = 120, mx: Float = -120, my: Float = -120, mz: Float = -120
        let start = Date()
        for i in 0..<points.vertices.count {
            var vertex = points.vertices[i] as! Primitive.Vertex
            
            switch (vertex.position.x, vertex.position.y, vertex.position.z) {
            case (x...maxX, y...maxY, z...maxZ):
                vertex.type = 1
                points.vertices[i] = vertex
                
                if xx > vertex.position.x { xx = vertex.position.x }
                if yy > vertex.position.y { yy = vertex.position.y }
                if zz > vertex.position.z { zz = vertex.position.z }
                if mx < vertex.position.x { mx = vertex.position.x }
                if my < vertex.position.y { my = vertex.position.y }
                if mz < vertex.position.z { mz = vertex.position.z }
            default:
                break
            }
        }
        print(Date().timeIntervalSince(start))
        points.buildBuffers()
        
        print(xx,yy,zz,mx,my,mz)
    }
    
}


extension PointCloudScene {
    
    func containCubeDetection() -> [Cube]? {
        
        guard let cubes = (points.children.filter { (node) -> Bool in
            guard let c = node as? Cube else { return false }
            if segmentedControl.isSelectedLeft {
                return c.fillMode == .fill
            } else {
                return c.fillMode == .lines
            }
        } as? [Cube]), cubes.count > 0, lines.count > 1 else { return nil }
        var selection = [Cube]()
        let modelMatrix = camera.viewMatrix * self.modelMatrix * points.modelMatrix
        let viewPort = simd_float4(0, 0, Float(size.width), Float(size.height))
        let positions = lines.map { viewSpaceToHorizontalPlane($0.vector2(), modelMatrix, camera.projectionMatrix, viewPort)!.xy }
        let matrixRotate = matrix_float2x2([0, 1], [-1,0])
        let scale = viewSpaceToWorldScale(Float(shapeLayer.lineWidth * 0.5), modelMatrix, camera.projectionMatrix, viewPort) ?? 1
        for cube in cubes {
            let cubeModelMatrix = cube.modelMatrix
            let minbounds = (cubeModelMatrix * vector4(cube.boundingBox.minBounds, 1)).xy
            let maxBounds = (cubeModelMatrix * vector4(cube.boundingBox.maxBounds, 1)).xy
            
            var start = positions[0]
            
            for end in positions[1..<lines.count] {
                var result = testLineSegmentIntersection(start: start, end: end, minBounds: minbounds, maxBounds: maxBounds)
                
                func changeCubeState(cube: Cube) {
                    if segmentedControl.isSelectedLeft  {
                        cube.fillMode = .lines
                        cube.color = SIMD4<Float>(1, 0, 0, 1)
                    } else {
                        cube.fillMode = .fill
                        cube.color = SIMD4<Float>(1, 1, 1, 1)
                    }
                    cube.buildVertices()
                    selection.append(cube)
                }
                
                if result {
                    changeCubeState(cube: cube)
                    break
                }
                
                let translation = normalize(matrixRotate * (end - start)) * scale
                
                var start1 = start + translation
                var end1 = end + translation
                result = testLineSegmentIntersection(start: start1, end: end1, minBounds: minbounds, maxBounds: maxBounds)
                if result {
                    changeCubeState(cube: cube)
                    break
                }
                
                start1 = start - translation
                end1 = end - translation
                result = testLineSegmentIntersection(start: start1, end: end1, minBounds: minbounds, maxBounds: maxBounds)
                if result {
                    changeCubeState(cube: cube)
                    break
                }
                
                start = end
            }
        }
        
        if l == nil {
            l = Lines(device: device)
        }
        l!.dataLines(line: [positions])
        points.add(childNode: l!)
        
        return selection
    }
    
    struct CubeBox {
        let maxBounds: simd_float3
        let minBounds: simd_float3
        let matrix: matrix_float4x4
        let id: Int32
        var type: Int32 = 0
        
        init(cube: Cube) {
            maxBounds = cube.boundingBox.maxBounds
            minBounds = cube.boundingBox.minBounds
            matrix = cube.modelMatrix
            id = Int32(cube.id)
            type = cube.fillMode == .fill ? 0 : 1
        }
    }
    
    func calcVertices(cubes: [Cube], line: [simd_float2], scale: Float) -> [Cube]? {
        guard cubes.count > 0 else { return nil }
        
        let boxes = cubes.map { CubeBox(cube: $0) }
        
        guard let library = device.makeDefaultLibrary(),
              let vertexFunction = library.makeFunction(name: "cube_intersect_change_type"),
              let commandQueue = device.makeCommandQueue(),
              let commandBuffer = commandQueue.makeCommandBuffer(),
              let commandEncoder = commandBuffer.makeComputeCommandEncoder(),
              let boxBuffer = device.makeBuffer(length:MemoryLayout<CubeBox>.stride * boxes.count,  options:MTLResourceOptions.storageModeShared),
              let lineBuffer = device.makeBuffer(length:MemoryLayout<simd_float2>.stride * line.count, options:MTLResourceOptions.storageModeShared),
              let countBuffer = device.makeBuffer(length:MemoryLayout<Int32>.stride * line.count,  options:MTLResourceOptions.storageModeShared),
              let scaleBuffer = device.makeBuffer(length:MemoryLayout<Float>.stride,  options:MTLResourceOptions.storageModeShared),
              let typeBuffer = device.makeBuffer(length:MemoryLayout<Int32>.stride,  options:MTLResourceOptions.storageModeShared) else { return nil }
        
        var pipeLineContains: MTLComputePipelineState
        do {
            pipeLineContains = try device.makeComputePipelineState(function: vertexFunction)
        } catch {
            return nil
        }
        
        commandEncoder.setComputePipelineState(pipeLineContains)
        
        boxes.withUnsafeBufferPointer { (p) in
            if let baseAddress = p.baseAddress {
                boxBuffer.contents().initializeMemory(as: CubeBox.self, from: baseAddress, count: p.count)
                commandEncoder.setBuffer(boxBuffer, offset: 0, index: 0)
            }
        }
        
        line.withUnsafeBufferPointer { (p) in
            if let baseAddress = p.baseAddress {
                lineBuffer.contents().initializeMemory(as: simd_float2.self, from: baseAddress, count: p.count)
                commandEncoder.setBuffer(lineBuffer, offset: 0, index: 1)
            }
        }
        
        let count32 = Int32(line.count)
        withUnsafeBytes(of: count32) { (p) in
            if let baseAddress = p.baseAddress {
                countBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Int32>.stride)
                commandEncoder.setBuffer(countBuffer, offset: 0, index: 2)
            }
        }
        
        withUnsafeBytes(of: scale) { (p) in
            if let baseAddress = p.baseAddress {
                scaleBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Float>.stride)
                commandEncoder.setBuffer(scaleBuffer, offset: 0, index: 3)
            }
        }
        let type: Int32 = segmentedControl.isSelectedLeft ? 1 : 0
        withUnsafeBytes(of: type) { (p) in
            if let baseAddress = p.baseAddress {
                typeBuffer.contents().copyMemory(from: UnsafeRawPointer(baseAddress), byteCount: MemoryLayout<Int32>.stride)
                commandEncoder.setBuffer(typeBuffer, offset: 0, index: 4)
            }
        }
        
        let threadsPerThreadgroup = MTLSize(width: pipeLineContains.threadExecutionWidth,height: 1,depth: 1)
        let threadgroupsCount = MTLSize(width: Int(ceil(Float(boxes.count) / Float(threadsPerThreadgroup.width))),
                                        height: 1,
                                        depth: 1)
        commandEncoder.dispatchThreadgroups(threadgroupsCount, threadsPerThreadgroup: threadsPerThreadgroup)
        commandEncoder.endEncoding()
        
        commandBuffer.commit()
        commandBuffer.waitUntilCompleted()
        
        func changeCubeState(cube: Cube) {
            if segmentedControl.isSelectedLeft  {
                cube.fillMode = .lines
                cube.color = SIMD4<Float>(1, 0, 0, 1)
            } else {
                cube.fillMode = .fill
                cube.color = SIMD4<Float>(1, 1, 1, 1)
            }
            cube.buildVertices()
            selection.append(cube)
        }
        let cubeboxes = boxBuffer.contents().bindMemory(to: CubeBox.self, capacity: boxes.count)
        var selection = [Cube]()
        for i in 0..<boxes.count {
            if cubeboxes[i].type == type {
                selection.append(cubes[i])
                changeCubeState(cube: cubes[i])
            }
        }
        return selection
    }
}
