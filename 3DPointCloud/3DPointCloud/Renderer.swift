//
//  Renderer.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  4:14 PM.
//    
//  

import MetalKit

class Renderer: NSObject {
    
    let device: MTLDevice
    let commandQueue: MTLCommandQueue
    
    var id: UInt? {
        return scene?.id
    }
    var scene: Scene?
    
    var depthStencilState: MTLDepthStencilState?
    
    init?(view: MTKView) {
        guard let mtlDevice = MTLCreateSystemDefaultDevice() else { return nil }
        device = mtlDevice
        view.device = mtlDevice
        
        view.clearColor = MTLClearColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        view.depthStencilPixelFormat = .depth32Float
        guard let queue = device.makeCommandQueue() else { return nil }
        commandQueue = queue
        
        super.init()
        view.delegate = self
        
        buildDepthStencilSate()
    }
    
    private func buildDepthStencilSate(){
        let depthStencilDescriptor = MTLDepthStencilDescriptor()
        depthStencilDescriptor.depthCompareFunction = .less
        depthStencilDescriptor.isDepthWriteEnabled = true
        depthStencilState = device.makeDepthStencilState(descriptor: depthStencilDescriptor)
    }
}

extension Renderer: MTKViewDelegate {
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        scene?.size = size
    }
    
    func draw(in view: MTKView) {
        guard let drawable = view.currentDrawable,
              let descriptor = view.currentRenderPassDescriptor,
              let commandBuffer = commandQueue.makeCommandBuffer(),
              let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: descriptor) else { return }
        
        commandEncoder.setDepthStencilState(depthStencilState)
        scene?.render(commandEncoder: commandEncoder)
        
        commandEncoder.endEncoding()
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
}
