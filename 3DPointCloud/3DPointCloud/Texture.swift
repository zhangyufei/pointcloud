//
//  Texture.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/19  5:40 PM.
//    
//  

import MetalKit

class Texture: Primitive {
    
    let textureURL: URL
    lazy var texture: MTLTexture? = {
        let textureLoader = MTKTextureLoader(device: device)
        let textureLoaderOptions: [MTKTextureLoader.Option : Any]?
        if #available(iOS 10.0, *) {
            textureLoaderOptions = [MTKTextureLoader.Option.origin : MTKTextureLoader.Origin.bottomLeft.rawValue]
        } else {
            textureLoaderOptions = nil
        }
        do {
            return try textureLoader.newTexture(URL: textureURL, options: textureLoaderOptions)
        } catch {
            return nil
        }
    }()
    
    init(device: MTLDevice, texture: URL) {
        textureURL = texture
        super.init(device: device)
        vertexFunctionName = "vertex_texture_shader"
        fragmentFunctionName = "fragment_texture_shader"
        vertexDescriptor = Texture.Vertex.vertexDescriptor
    }
    
    override func buildBuffers() {
        assert(!vertices.isEmpty, "顶点数据不能为空")
        let vertices = self.vertices as! [Texture.Vertex]
        vertexBuffer = device.makeBuffer(bytes: vertices,
                                         length: vertices.count *
                                            MemoryLayout<Texture.Vertex>.stride,
                                         options: [])
        
        if !indices.isEmpty {
            indexBuffer = device.makeBuffer(bytes: indices,
                                            length: indices.count * MemoryLayout<UInt16>.size,
                                            options: [])
        }
    }
    
    override func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        guard let indexBuffer = indexBuffer,
              let pipelineState = pipelineState else { return }
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setVertexBuffer(vertexBuffer,
                                       offset: 0, index: 0)
        withUnsafePointer(to: uniforms) { (p) in
            commandEncoder.setVertexBytes(UnsafeRawPointer(p),
                                          length: MemoryLayout<Uniforms>.stride,
                                          index: 1)
        }
        commandEncoder.setFragmentSamplerState(samplerState, index: 0)
        commandEncoder.setFragmentTexture(texture, index: 0)
        commandEncoder.setFrontFacing(.counterClockwise)
        commandEncoder.setCullMode(.back)
        commandEncoder.drawIndexedPrimitives(type: .triangle,
                                             indexCount: indices.count,
                                             indexType: .uint16,
                                             indexBuffer: indexBuffer,
                                             indexBufferOffset: 0)
    }
}

extension Texture {
    struct Vertex {
        var position: SIMD3<Float>
        var color: SIMD4<Float>
        var texture: SIMD2<Float>
        
        static var vertexDescriptor: MTLVertexDescriptor = {
            let vertexDescriptor = MTLVertexDescriptor()
            
            vertexDescriptor.attributes[0].format = .float3
            vertexDescriptor.attributes[0].offset = 0
            vertexDescriptor.attributes[0].bufferIndex = 0
            
            vertexDescriptor.attributes[1].format = .float4
            vertexDescriptor.attributes[1].offset = MemoryLayout<SIMD3<Float>>.stride
            vertexDescriptor.attributes[1].bufferIndex = 0
            
            vertexDescriptor.attributes[2].format = .float2
            vertexDescriptor.attributes[2].offset = MemoryLayout<SIMD3<Float>>.stride + MemoryLayout<SIMD4<Float>>.stride
            vertexDescriptor.attributes[2].bufferIndex = 0
            
            vertexDescriptor.layouts[0].stride = MemoryLayout<Vertex>.stride
            
            return vertexDescriptor
        }()
    }
}


extension Texture {
    func testData() {
        
        let red = SIMD4<Float>(1, 0, 0, 1)
//        let orange = SIMD4<Float>(1, 165/255, 0, 1)
        let yellow = SIMD4<Float>(1, 1, 0, 1)
        let green = SIMD4<Float>(0, 1, 0, 1)
//        let cyan = SIMD4<Float>(0, 1, 1, 1)
        let blue = SIMD4<Float>(0, 0, 1, 1)
//        let purple = SIMD4<Float>(128/255, 0, 128/255, 1)
//        let white = SIMD4<Float>(1, 1, 1, 1)
        let h: Float = 50
        let w: Float = h * 1920 / 1080 * 0.5
        vertices = [Texture.Vertex(position: SIMD3<Float>(-w, 0, -1), color: red,
                                   texture: SIMD2<Float>(0,0)),
                    Texture.Vertex(position: SIMD3<Float>(w, 0, -1), color: yellow,
                                   texture: SIMD2<Float>(1,0)),
                    Texture.Vertex(position: SIMD3<Float>(w, h, -1), color: green,
                                   texture: SIMD2<Float>(1,1)),
                    Texture.Vertex(position: SIMD3<Float>(-w, h, -1), color: blue,
                                   texture: SIMD2<Float>(0,1))]
        indices = [0,1,2, 2,3,0]
        buildBuffers()
    }
}

extension MTLTexture {
    
    func bytes() -> UnsafeMutableRawPointer {
        let width = self.width
        let height   = self.height
        let rowBytes = self.width * 4
        let count = width * height * 4
        let p = malloc(count)
        
        self.getBytes(p!, bytesPerRow: rowBytes, from: MTLRegionMake2D(0, 0, width, height), mipmapLevel: 0)
        
        if let arr = p?.bindMemory(to: UInt8.self, capacity: count) {
            for i in 0..<count/4 {
                if arr[i] == 0,arr[i+1] == 0,arr[i+2] == 0 {
                    arr[i+3] = 0
                }
            }
        }
        
        return p!
    }
    
    func toImage() -> CGImage? {
        let p = bytes()
        
        let pColorSpace = CGColorSpaceCreateDeviceRGB()
        
        let rawBitmapInfo = CGImageAlphaInfo.noneSkipFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
        let bitmapInfo:CGBitmapInfo = CGBitmapInfo(rawValue: rawBitmapInfo)
        
        let selftureSize = self.width * self.height * 4
        let rowBytes = self.width * 4
        let releaseMaskImagePixelData: CGDataProviderReleaseDataCallback = { (info: UnsafeMutableRawPointer?, data: UnsafeRawPointer, size: Int) -> () in
            return
        }
        let provider = CGDataProvider(dataInfo: nil, data: p, size: selftureSize, releaseData: releaseMaskImagePixelData)
        
        let cgImageRef = CGImage(width: self.width, height: self.height, bitsPerComponent: 8, bitsPerPixel: 32, bytesPerRow: rowBytes, space: pColorSpace, bitmapInfo: bitmapInfo, provider: provider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)!
        
        return cgImageRef
    }
}
