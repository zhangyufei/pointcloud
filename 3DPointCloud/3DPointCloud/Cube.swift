//
//  Cube.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/20  3:26 PM.
//    
//  

import MetalKit

class Cube: Primitive {
    
    struct Position {
        var origin = SIMD3<Float>(repeating: 0)
        /// 分别对应X、Y、Z轴
        var size = SIMD3<Float>(repeating: 0)
        
        func insetBy(dx: Float, dy: Float, dz: Float) -> Position {
            guard dx*2 < size.x,
                  dy*2 < size.y,
                  dz*2 < size.z else { return self }
            var p = Position()
            p.origin.x -= dx
            p.origin.y -= dy
            p.origin.z -= dz
            
            p.size.x -= dx*2
            p.size.y -= dy*2
            p.size.z -= dz*2
            
            return p
        }
        
        mutating func transform(boundingBox: MDLAxisAlignedBoundingBox) {
            origin = boundingBox.minBounds
            size = boundingBox.maxBounds - boundingBox.minBounds
        }
    }
    /// 三维坐标系的坐标 MTLRegion
    var frame = Position()
    
    var boundingBox: MDLAxisAlignedBoundingBox {
        let p = frame.origin
        let q = frame.origin + frame.size
        let mmin = min(p, q)
        let mmax = max(p, q)
        return MDLAxisAlignedBoundingBox(maxBounds: mmax, minBounds: mmin)
    }
    
    var color = SIMD4<Float>(1, 1, 1, 1)
    
    override init(device: MTLDevice) {
        super.init(device: device)
        vertexFunctionName = "vertex_lines_shader"
        fragmentFunctionName = "fragment_cube_shader"
        indices = [0,1, 1,2, 2,3, 3,0,
                   4,5, 5,6, 6,7, 7,4,
                   0,4, 1,5, 2,6, 3,7]
        indexBuffer = device.makeBuffer(bytes: indices,
                                        length: indices.count * MemoryLayout<UInt16>.size,
                                        options: [])
    }
    
    func buildVertices() {
        vertices.removeAll()
        vertices.append(Primitive.Vertex(position: frame.origin, color: color))
        let position2 = SIMD3<Float>(frame.origin.x + frame.size.x,
                                     frame.origin.y,
                                     frame.origin.z)
        vertices.append(Primitive.Vertex(position: position2, color: color))
        let position3 = SIMD3<Float>(frame.origin.x + frame.size.x,
                                     frame.origin.y + frame.size.y,
                                     frame.origin.z)
        vertices.append(Primitive.Vertex(position: position3, color: color))
        let position4 = SIMD3<Float>(frame.origin.x,
                                     frame.origin.y + frame.size.y,
                                     frame.origin.z)
        vertices.append(Primitive.Vertex(position: position4, color: color))
        
        let position5 = SIMD3<Float>(frame.origin.x,
                                     frame.origin.y,
                                     frame.origin.z + frame.size.z)
        vertices.append(Primitive.Vertex(position: position5, color: color))
        let position6 = SIMD3<Float>(frame.origin.x + frame.size.x,
                                     frame.origin.y,
                                     frame.origin.z + frame.size.z)
        vertices.append(Primitive.Vertex(position: position6, color: color))
        let position7 = SIMD3<Float>(frame.origin.x + frame.size.x,
                                     frame.origin.y + frame.size.y,
                                     frame.origin.z + frame.size.z)
        vertices.append(Primitive.Vertex(position: position7, color: color))
        let position8 = SIMD3<Float>(frame.origin.x,
                                     frame.origin.y + frame.size.y,
                                     frame.origin.z + frame.size.z)
        vertices.append(Primitive.Vertex(position: position8, color: color))
        
        guard let vertices = vertices as? [Primitive.Vertex] else { return }
        vertexBuffer = device.makeBuffer(bytes: vertices,
                                         length: vertices.count *
                                            MemoryLayout<Primitive.Vertex>.stride,
                                         options: [])
    }
    
    override func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        guard let indexBuffer = indexBuffer,
              let pipelineState = pipelineState  else { return }
        
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setVertexBuffer(vertexBuffer,
                                       offset: 0, index: 0)
        withUnsafePointer(to: uniforms) { (p) in
            commandEncoder.setVertexBytes(UnsafeRawPointer(p),
                                          length: MemoryLayout<Uniforms>.stride,
                                          index: 1)
        }
        
        withUnsafePointer(to: color) { (p) in
            commandEncoder.setFragmentBytes(UnsafeRawPointer(p),
                                            length: MemoryLayout<SIMD4<Float>>.stride,
                                            index: 1)
        }
        
        commandEncoder.drawIndexedPrimitives(type: .line,
                                             indexCount: indices.count,
                                             indexType: .uint16,
                                             indexBuffer: indexBuffer,
                                             indexBufferOffset: 0)
    }
    
    override func hitTestWithSegment(ray: Ray, transform: matrix_float4x4) -> Float? {
        guard isUserInteractionEnabled else { return nil }
        let localRay = transform.inverse * ray
        guard let t = testRayAABBIntersection(boundingBox: boundingBox, ray: localRay) else { return nil }
        
        var intersection = localRay.extrapolate(t)
        intersection = (transform * SIMD4<Float>(intersection, 1)).xyz
        let distance = ray.interpolate(intersection)
        return distance < 0 ? nil : distance
    }
}

extension Cube {
    
    func testData() {
        frame.origin = SIMD3<Float>(-3.265519,  -0.22041075,  -0.17148186)
        frame.size.x = 0.5146423
        frame.size.y = 0.57012872
        frame.size.z = 1.52792986
        
        buildVertices()
    }
    
    func dataYellow() {
        color = SIMD4<Float>(1, 1, 0, 1)
        frame.origin = SIMD3<Float>(0,  0,  0)
        frame.size.x = 1
        frame.size.y = 2
        frame.size.z = 3
        
        buildVertices()
    }
    
    func dataGreen() {
        color = SIMD4<Float>(0, 1, 0, 1)
        frame.origin = SIMD3<Float>(0,  0,  0)
        frame.size.x = 2
        frame.size.y = 4
        frame.size.z = 10
        
        buildVertices()
    }
    
    func addShadow() {
        let cube = Cube(device: device)
        cube.color = SIMD4<Float>(0, 1, 1, 1)
        cube.frame.origin = boundingBox.minBounds + vector3(2, 2, 0)
        cube.frame.size = boundingBox.maxBounds - boundingBox.minBounds
        cube.buildVertices()
        cube.isUserInteractionEnabled = false
        children.append(cube)
    }
}
