//
//  Lines.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/19  3:24 PM.
//    
//  

import MetalKit

class Lines: Primitive {
    
    var primitiveType: MTLPrimitiveType = .line
    
    override init(device: MTLDevice) {
        super.init(device: device)
        vertexFunctionName = "vertex_lines_shader"
    }
    
    override func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        guard let indexBuffer = indexBuffer,
              let pipelineState = pipelineState  else { return }
        
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setVertexBuffer(vertexBuffer,
                                       offset: 0, index: 0)
        withUnsafePointer(to: uniforms) { (p) in
            commandEncoder.setVertexBytes(UnsafeRawPointer(p),
                                          length: MemoryLayout<Uniforms>.stride,
                                          index: 1)
        }
        
        commandEncoder.drawIndexedPrimitives(type: primitiveType,
                                             indexCount: indices.count,
                                             indexType: .uint16,
                                             indexBuffer: indexBuffer,
                                             indexBufferOffset: 0)
    }
}

extension Lines {
    func testData() {
        
        let red = SIMD4<Float>(1, 0, 0, 1)
        let orange = SIMD4<Float>(1, 165/255, 0, 1)
        let yellow = SIMD4<Float>(1, 1, 0, 1)
        let green = SIMD4<Float>(0, 1, 0, 1)
        let cyan = SIMD4<Float>(0, 1, 1, 1)
        let blue = SIMD4<Float>(0, 0, 1, 1)
        let purple = SIMD4<Float>(128/255, 0, 128/255, 1)
        let white = SIMD4<Float>(1, 1, 1, 1)
        vertices = [Primitive.Vertex(position: SIMD3<Float>(-125, -125, -1), color: red),
                    Primitive.Vertex(position: SIMD3<Float>(125, -125, -1), color: orange),
                    Primitive.Vertex(position: SIMD3<Float>(125, 125, -1), color: yellow),
                    Primitive.Vertex(position: SIMD3<Float>(-125, 125, -1), color: green),
                    
                    Primitive.Vertex(position: SIMD3<Float>(-125, -125, 45), color: cyan),
                    Primitive.Vertex(position: SIMD3<Float>(125, -125, 45), color: blue),
                    Primitive.Vertex(position: SIMD3<Float>(125, 125, 45), color: purple),
                    Primitive.Vertex(position: SIMD3<Float>(-125, 125, 45), color: white)]
        indices = [0,1, 1,2, 2,3, 3,0,
                   4,5, 5,6, 6,7, 7,4,
                   0,4, 1,5, 2,6, 3,7]
        buildBuffers()
    }
    /// 法线
    func dataNormal() {
        let red = SIMD4<Float>(1, 0, 0, 1)
        let green = SIMD4<Float>(0, 1, 0, 1)
        let blue = SIMD4<Float>(1, 1, 0, 1)
        vertices = [Primitive.Vertex(position: SIMD3<Float>(0, 0, 0), color: red),
                    Primitive.Vertex(position: SIMD3<Float>(3, 0, 0), color: red),
                    Primitive.Vertex(position: SIMD3<Float>(0, 0, 0), color: green),
                    Primitive.Vertex(position: SIMD3<Float>(0, 3, 0), color: green),
                    Primitive.Vertex(position: SIMD3<Float>(0, 0, 0), color: blue),
                    Primitive.Vertex(position: SIMD3<Float>(0, 0, 3), color: blue)]
        indices = [0,1, 2,3, 4,5]
        buildBuffers()
    }
    
    func dataRay(near: simd_float3, far: simd_float3) {
        let red = SIMD4<Float>(1, 0, 0, 1)
        let blue = SIMD4<Float>(1, 1, 0, 1)
        vertices = [Primitive.Vertex(position: near, color: red),
                    Primitive.Vertex(position: far, color: blue)]
        indices = [0,1]
        buildBuffers()
    }
    
    func dataLines(line: [[simd_float2]]) {
        guard line.count > 0 else { return }
        vertices.removeAll()
        indices.removeAll()
        let green = SIMD4<Float>(1, 1, 0, 1)
        var index: UInt16 = 0
        for i in 0..<line.count {
            let l = line[i]
            guard l.count > 1 else { continue }
            vertices.append(Primitive.Vertex(position: simd_float3(l[0], 0), color: green))
            index += 1
            for p in 1..<l.count {
                let vertex = simd_float3(l[p], 0)
                vertices.append(Primitive.Vertex(position: vertex, color: green))
                indices.append(index - 1)
                indices.append(index)
                index += 1
            }
        }
        
        buildBuffers()
    }
}
