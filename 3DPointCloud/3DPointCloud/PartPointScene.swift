//
//  PartPointScene.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/4/23  4:02 PM.
//    
//  

import MetalKit

class PartPointScene: Scene {
    
    let points: Points
    let cube: Cube
    var viewType: ViewType = .overlook
    
    init(device: MTLDevice, size: CGSize, view: MTKView, points: Points, cube: Cube) {
        
        self.points = points
        self.cube = cube
        
        super.init(device: device, size: size, view: view)
        points.add(childNode: cube)
        add(childNode: points)
        
        setGestureRecognizer()
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tap3.numberOfTapsRequired = 3
        view.addGestureRecognizer(tap3)
        
    }
    
    /// 开始移动之前的信息
    private var package: (beganPoint: CGPoint, activeDotPosition: CGRect.Position, beginRect: CGRect) = (CGPoint.zero, .leftTop(CGPoint.zero), CGRect.zero)
    lazy var borderLayer: CALayer = {
        let borderLayer = CALayer()
        borderLayer.backgroundColor = UIColor.red.withAlphaComponent(0.2).cgColor
        borderLayer.borderWidth = 1
        borderLayer.borderColor = UIColor.white.cgColor
        return borderLayer
    }()
    
    func displayBorder() {
        
        let matrix = camera.viewProjectionMatrix * modelMatrix * points.modelMatrix * cube.modelMatrix
        
        var min = cube.frame.origin, max = cube.frame.origin + cube.frame.size
        min = (matrix * SIMD4<Float>(min, 1)).xyz
        max = (matrix * SIMD4<Float>(max, 1)).xyz
        
        var p1 = CGPoint(x: (CGFloat(min.x) + 1) * 0.5 * size.width, y: (1 - ((CGFloat(min.y) + 1) * 0.5)) * size.height)
        var p2 = CGPoint(x: (CGFloat(max.x) + 1) * 0.5 * size.width, y: (1 - ((CGFloat(max.y) + 1) * 0.5)) * size.height)
        switch viewType {
        case .overlook:
            break
        case .front:
            break
        case .side:
            break
        }
        
        if p1.x > p2.x { swap(&p1.x, &p2.x)}
        if p1.y > p2.y { swap(&p1.y, &p2.y)}
        
        borderLayer.frame = CGRect(origin: p1, size: CGSize(width: p2.x - p1.x, height: p2.y - p1.y))
        view.layer.addSublayer(borderLayer)
    }
    
    @objc override func handlePanGestureRecognizer(_ pan: UIPanGestureRecognizer) {

        let translation = pan.translation(in: view)
//        if selectNode != nil {
//            handleNodeRotate(translation)
//            pan.setTranslation(.zero, in: pan.view)
//        } else {
        super.handlePanGestureRecognizer(pan)
//        }

    }

    
    /// 处理长按手势
    @objc override func handleLongPressGestureRecognizer(_ press: UILongPressGestureRecognizer) {
        guard borderLayer.superlayer != nil else { return press.state = .failed }
        if press.numberOfTouchesRequired == 1 {
            let p = press.location(in: view)
            
            switch press.state {
            case .began:
                let frame = borderLayer.frame
                package.activeDotPosition = frame.minDistance(point: p)
                package.beginRect = frame
                package.beganPoint = p
                borderLayer.backgroundColor = UIColor.clear.cgColor
            case .changed:
                borderLayer.frame = package.beginRect.changeFame(p - package.beganPoint, position: package.activeDotPosition)
//            case .ended:
//                break
            default:
                borderLayer.backgroundColor = UIColor.red.withAlphaComponent(0.3).cgColor
                break
            }
        }
    }

}
extension PartPointScene {
    
    /// 点击处理
    @objc func handleTapGestureRecognizer(_ tap: UITapGestureRecognizer) {
        
        if tap.numberOfTapsRequired == 1 {
            let p = tap.location(in: view)
            
        } else if tap.numberOfTapsRequired == 2 {
            
            
        } else if tap.numberOfTapsRequired == 3 {
            view.removeFromSuperview()
        }
    }
}
extension PartPointScene {
    enum ViewType {
        case overlook
        case front
        case side
    }
    
}
