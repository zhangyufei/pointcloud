//
//  Extension.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/4/25  12:06 PM.
//    
//  

import UIKit
import ModelIO.MDLTypes

extension CGRect {
    
    enum Position {
        case leftTop(CGPoint)
        case rightTop(CGPoint)
        case rightBottom(CGPoint)
        case leftBottom(CGPoint)
        case topMid(CGPoint)
        case bottomMid(CGPoint)
        case leftMid(CGPoint)
        case rightMid(CGPoint)
        
        var point: CGPoint {
            switch self {
            case .leftTop(let p):
                return p
            case .rightTop(let p):
                return p
            case .rightBottom(let p):
                return p
            case .leftBottom(let p):
                return p
            case .topMid(let p):
                return p
            case .bottomMid(let p):
                return p
            case .leftMid(let p):
                return p
            case .rightMid(let p):
                return p
            }
        }
    }
    
    var leftTop: CGPoint {
        return origin
    }
    var rightTop: CGPoint {
        return CGPoint(x: maxX, y: origin.y)
    }
    var leftBottom: CGPoint {
        return CGPoint(x: origin.x, y: maxY)
    }
    var rightBottom: CGPoint {
        return CGPoint(x: maxX, y: maxY)
    }
    var topMid: CGPoint {
        return CGPoint(x: midX, y: origin.y)
    }
    var bottomMid: CGPoint {
        return CGPoint(x: midX, y: maxY)
    }
    var leftMid: CGPoint {
        return CGPoint(x: origin.x, y: midY)
    }
    var rightMid: CGPoint {
        return CGPoint(x: maxX, y: midY)
    }
    var center: CGPoint {
        return CGPoint(x: midX, y: midY)
    }
    
    var allPosition: [Position] {
        return [.leftTop(leftTop), .topMid(topMid),
                .rightTop(rightTop), .leftMid(leftMid),
                .rightMid(rightMid), .leftBottom(leftBottom),
                .bottomMid(bottomMid), .rightBottom(rightBottom)]
    }
    
    /// 转换成UIBezierPath，clockwise: 顺时针方向
    func toBezierPath(_ clockwise: Bool = true) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: leftTop)
        if clockwise {
            path.addLine(to: rightTop)
            path.addLine(to: rightBottom)
            path.addLine(to: leftBottom)
        } else {
            path.addLine(to: leftBottom)
            path.addLine(to: rightBottom)
            path.addLine(to: rightTop)
        }
        path.addLine(to: leftTop)
        path.close()
        return path
    }
    /// 矩形周长
    var perimeter: CGFloat {
        return (size.width + size.height) * 2
    }
    /// 返回是否包含指定的点 包括等于边界的情况
    func containsClosedRange(_ point: CGPoint) -> Bool {
        guard maxX > origin.x, maxY > origin.y else { return false }
        switch (point.x, point.y) {
        case (origin.x...maxX, origin.y...maxY):
            return true
        default: return false
        }
    }
    /// 如果指定的点在矩形区域外部，将指定点移动到距离最近的边上，有0.1的容错处理
    func adsorb(_ point: CGPoint) -> CGPoint? {
        var change = false
        var p = point
        if p.x < origin.x {
            p.x = origin.x + 0.1
            change = true
        }
        if p.y < origin.y {
            p.y = origin.y + 0.1
            change = true
        }
        if p.x > maxX {
            p.x = maxX - 0.1
            change = true
        }
        if p.y > maxY {
            p.y = maxY - 0.1
            change = true
        }
        return change ? p : nil
    }
    
    /// 获取距离指定点最小距离的控制点
    func minDistance(point: CGPoint) -> CGRect.Position {
        var minDistanceDot = CGRect.Position.leftTop(leftTop)
        var minDistance = point.distance(minDistanceDot.point)
        for item in allPosition[1...7] {
            let distance = point.distance(item.point)
            if distance < minDistance {
                minDistance = distance
                minDistanceDot = item
            }
        }
        
        switch minDistanceDot {
        case .leftTop(_), .rightTop(_), .leftBottom(_), .rightBottom(_):
            var dotAreas = [(CGRect.Position, UIBezierPath)]()
            
            let maxInset: CGFloat = 29
            let insetx = -min(size.width*0.5, maxInset)
            let insety = -min(size.height*0.5, maxInset)
            let maxW = -size.width*3/8 + 1
            let maxH = -size.height*3/8 + 1
            
            var inset = UIEdgeInsets(top: maxInset, left: maxW,
                                     bottom: insety, right: maxW)
            let topMid = CGRect(x: midX - 1,
                                y: origin.y - 1,
                                width: 2, height: 2).inset(by: inset).toBezierPath()
            dotAreas.append((.topMid(self.topMid), topMid))
            
            inset = UIEdgeInsets(top: insety, left: maxW,
                                 bottom: maxInset, right: maxW)
            let bottomMid = CGRect(x: midX - 1,
                                   y: maxY - 1,
                                   width: 2, height: 2).inset(by: inset).toBezierPath()
            dotAreas.append((.bottomMid(self.bottomMid), bottomMid))
            
            inset = UIEdgeInsets(top: maxH, left: maxInset,
                                 bottom: maxH, right: insetx)
            let leftMid = CGRect(x: origin.x - 1,
                                 y: midY - 1,
                                 width: 2, height: 2).inset(by: inset).toBezierPath()
            dotAreas.append((.leftMid(self.leftMid), leftMid))
            
            inset = UIEdgeInsets(top: maxH, left: insetx,
                                 bottom: maxH, right: maxInset)
            let rightMid = CGRect(x: maxX - 1,
                                  y: midY - 1,
                                  width: 2, height: 2).inset(by: inset).toBezierPath()
            dotAreas.append((.rightMid(self.rightMid), rightMid))
            
            for item in dotAreas {
                if item.1.contains(point) {
                    minDistanceDot = item.0
                }
            }
        default: break
        }
        return minDistanceDot
    }
}

extension CGRect {
    
    func changeFame(_ translation: CGPoint, position: Position) -> CGRect {
        var frame = self
        switch position {
        case .leftTop(_):
            frame.origin.x += translation.x
            frame.origin.y += translation.y
            frame.size.width -= translation.x
            frame.size.height -= translation.y
        case .rightTop(_):
            frame.origin.y += translation.y
            frame.size.width += translation.x
            frame.size.height -= translation.y
        case .rightBottom(_):
            frame.size.width += translation.x
            frame.size.height += translation.y
        case .leftBottom(_):
            frame.origin.x += translation.x
            frame.size.width -= translation.x
            frame.size.height += translation.y
        case .topMid(_):
            frame.origin.y += translation.y
            frame.size.height -= translation.y
        case .bottomMid(_):
            frame.size.height += translation.y
        case .leftMid(_):
            frame.origin.x += translation.x
            frame.size.width -= translation.x
        case .rightMid(_):
            frame.size.width += translation.x
        }
        return frame
    }
}

extension CGPoint {
    /// 两个点之间的距离
    func distance(_ other: CGPoint) -> CGFloat {
        let sx = other.x - x
        let sy = other.y - y
        return sqrt(sx*sx + sy*sy)
    }
    
    /// 检查本身是否超出了指定size的范围
    /// - Parameter size: 指定的范围
    /// - Returns: 返回校正过的值
    func edgeInspect(_ size: CGSize) -> CGPoint {
        var point = self
        if point.x < 0 {
            point.x = 0
        } else if point.x > size.width {
            point.x = size.width
        }
        if point.y < 0 {
            point.y = 0
        } else if point.y > size.height {
            point.y = size.height
        }
        return point
    }
    
    /// 两个点相加的结果 第二个点相当于基于第一个点移动的距离
    static func +(left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x, y: left.y + right.y)
    }
    /// 两个点相减构成一个有向向量
    static func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
    }
    /// 乘以标量
    static func *(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
        return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
    }
    /// 除以标量
    static func /(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
        return CGPoint(x: lhs.x / rhs, y: lhs.y / rhs)
    }
}

extension CGPoint {
    func vector2() -> SIMD2<Float> {
        return SIMD2<Float>(Float(x), Float(y))
    }
}

extension Array where Element == Primitive.Vertex {
    
    func boundingBox(_ matrix: matrix_float4x4) -> MDLAxisAlignedBoundingBox? {
        guard let first = self.first else { return nil }
        var min = (matrix * SIMD4<Float>(first.position, 1)).xyz
        var max = min
        for item in self {
            let position = matrix * SIMD4<Float>(item.position, 1)
            
            if position.x < min.x { min.x = position.x }
            if position.y < min.y { min.y = position.y }
            if position.z < min.z { min.z = position.z }
            
            if position.x > max.x { max.x = position.x }
            if position.y > max.y { max.y = position.y }
            if position.z > max.z { max.z = position.z }
        }
        
        return MDLAxisAlignedBoundingBox(maxBounds: max, minBounds: min)
    }
}
