//
//  Submesh.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/2/19  5:44 PM.
//    
//  

import MetalKit

class Submesh {
    
    var mtkSubmesh: MTKSubmesh
    let textures: Textures
    
    init(mtkSubmesh: MTKSubmesh, textures: Textures) {
        self.mtkSubmesh = mtkSubmesh
        self.textures = textures
    }
}

extension Submesh {
    
    static func filename(mdlSubmesh: MDLSubmesh?) -> String? {
        guard let property = mdlSubmesh?.material?.property(with: MDLMaterialSemantic.baseColor),
              property.type == .string else { return nil }
        return property.stringValue
    }
    
    struct Textures {
        let baseColor: MTLTexture?
        
        init(filename: String) {
            baseColor = try? Primitive.loadTexture(imageName: filename)
        }
        
        init() {
            baseColor = nil
        }
    }
}
