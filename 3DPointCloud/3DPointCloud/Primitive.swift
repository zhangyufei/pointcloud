//
//  Primitive.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  2:45 PM.
//
//  

import MetalKit

class Primitive: Node {
    
    var vertices: [Any] = []
    var indices: [UInt16] = []
    
    var vertexBuffer: MTLBuffer?
    var indexBuffer: MTLBuffer?
    
    var modelViewMatrix = matrix_identity_float4x4
    
    var vertexFunctionName: String = "vertex_shader"
    var fragmentFunctionName: String = "fragment_shader"
    var colorPixelFormat: MTLPixelFormat = .bgra8Unorm
    var vertexDescriptor: MTLVertexDescriptor = Primitive.Vertex.vertexDescriptor
    lazy var pipelineState: MTLRenderPipelineState? = {
        
        guard let library = device.makeDefaultLibrary() else { return nil }
        let vertexFunction = library.makeFunction(name: vertexFunctionName)
        let fragmentFunction = library.makeFunction(name: fragmentFunctionName)
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        pipelineDescriptor.colorAttachments[0].pixelFormat = colorPixelFormat
        pipelineDescriptor.vertexDescriptor = vertexDescriptor
        pipelineDescriptor.depthAttachmentPixelFormat = .depth32Float
//        pipelineDescriptor.sampleCount = 4
        do {
            return try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            return nil
        }
    }()
    
    lazy var samplerState: MTLSamplerState? = {
        let descriptor = MTLSamplerDescriptor()
        descriptor.minFilter = .linear
        descriptor.magFilter = .linear
        return device.makeSamplerState(descriptor: descriptor)
    }()
    
    init(device: MTLDevice) {
        super.init(mtlDevice: device)
    }
    
    func buildBuffers() {
        assert(!vertices.isEmpty, "顶点数据不能为空")
        let vertices = self.vertices as! [Vertex]
        vertexBuffer = device.makeBuffer(bytes: vertices,
                                         length: vertices.count *
                                            MemoryLayout<Vertex>.stride,
                                         options: MTLResourceOptions.storageModeShared)
        
        if !indices.isEmpty {
            indexBuffer = device.makeBuffer(bytes: indices,
                                            length: indices.count * MemoryLayout<UInt16>.size,
                                            options: [])
        }
    }
    
    func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        
    }
}

extension Primitive {
    struct Vertex {
        var position: SIMD3<Float>
        var color: SIMD4<Float>
        var type: Int32 = 0
        
        static var vertexDescriptor: MTLVertexDescriptor = {
            let vertexDescriptor = MTLVertexDescriptor()
            
            vertexDescriptor.attributes[0].format = .float3
            vertexDescriptor.attributes[0].offset = 0
            vertexDescriptor.attributes[0].bufferIndex = 0
            
            vertexDescriptor.attributes[1].format = .float4
            vertexDescriptor.attributes[1].offset = MemoryLayout<SIMD3<Float>>.stride
            vertexDescriptor.attributes[1].bufferIndex = 0
            
            vertexDescriptor.attributes[2].format = .int
            vertexDescriptor.attributes[2].offset = MemoryLayout<SIMD3<Float>>.stride + MemoryLayout<SIMD4<Float>>.stride
            vertexDescriptor.attributes[2].bufferIndex = 0
            
            vertexDescriptor.layouts[0].stride = MemoryLayout<Vertex>.stride
            
            return vertexDescriptor
        }()
    }
}

extension Primitive {
    
    static func loadTexture(imageName: String) throws -> MTLTexture? {
        guard let device = MTLCreateSystemDefaultDevice() else { return nil }
        let textureLoader = MTKTextureLoader(device: device)
        var textureLoaderOptions: [MTKTextureLoader.Option: Any] = [.SRGB: false]
        if #available(iOS 10.0, *) {
            textureLoaderOptions[.origin] = MTKTextureLoader.Origin.bottomLeft.rawValue
            textureLoaderOptions[.generateMipmaps] = NSNumber(booleanLiteral: true)
        }
        
        let fileExtension = URL(fileURLWithPath: imageName).pathExtension.isEmpty ? "png" : nil
        
        let texture: MTLTexture?
        if let url = Bundle.main.url(forResource: imageName, withExtension: fileExtension) {
            texture = try? textureLoader.newTexture(URL: url, options: textureLoaderOptions)
        } else {
            if #available(iOS 10.0, *) {
                texture = try? textureLoader.newTexture(name: imageName,
                                                        scaleFactor: 1.0,
                                                        bundle: Bundle.main, options: nil)
            } else {
                texture = nil
            }
        }
        
        return texture
      }
}


