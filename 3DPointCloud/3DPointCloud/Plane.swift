//
//  Plane.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/19  5:41 PM.
//    
//  

import MetalKit

class Plane: Primitive {
    
    override init(device: MTLDevice) {
        super.init(device: device)
    }
    
    override func doRender(commandEncoder: MTLRenderCommandEncoder, uniforms: Uniforms) {
        guard let indexBuffer = indexBuffer,
              let pipelineState = pipelineState else { return }
        
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setVertexBuffer(vertexBuffer,
                                       offset: 0, index: 0)
        withUnsafePointer(to: uniforms) { (p) in
            commandEncoder.setVertexBytes(UnsafeRawPointer(p),
                                          length: MemoryLayout<Uniforms>.stride,
                                          index: 1)
        }
        
        commandEncoder.drawIndexedPrimitives(type: .triangle,
                                             indexCount: indices.count,
                                             indexType: .uint16,
                                             indexBuffer: indexBuffer,
                                             indexBufferOffset: 0)
    }
}

extension Plane {
    @objc func testData() {
        
        let red = SIMD4<Float>(1, 0, 0, 1)
//        let orange = SIMD4<Float>(1, 165/255, 0, 1)
        let yellow = SIMD4<Float>(1, 1, 0, 1)
        let green = SIMD4<Float>(0, 1, 0, 1)
//        let cyan = SIMD4<Float>(0, 1, 1, 1)
        let blue = SIMD4<Float>(0, 0, 1, 1)
//        let purple = SIMD4<Float>(128/255, 0, 128/255, 1)
//        let white = SIMD4<Float>(1, 1, 1, 1)
        let w: Float = 60 * 1920 / 1080 * 0.5
        let h: Float = 60
        vertices = [Primitive.Vertex(position: SIMD3<Float>(-w, 0, -1), color: red),
                    Primitive.Vertex(position: SIMD3<Float>(w, 0, -1), color: yellow),
                    Primitive.Vertex(position: SIMD3<Float>(w, h, -1), color: green),
                    Primitive.Vertex(position: SIMD3<Float>(-w, h, -1), color: blue)]
        indices = [0,1,2, 2,3,0]
        buildBuffers()
    }
}
