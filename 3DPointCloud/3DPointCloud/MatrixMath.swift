//
//  MatrixMath.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  2:14 PM.
//    
//  

import simd
import ModelIO.MDLTypes
import GLKit.GLKMathUtils

struct Ray {
    var origin: SIMD3<Float>
    var direction: SIMD3<Float>
    
    static func *(transform: float4x4, ray: Ray) -> Ray {
        let originT = (transform * SIMD4<Float>(ray.origin, 1)).xyz
        let directionT = (transform * SIMD4<Float>(ray.direction, 0)).xyz
        return Ray(origin: originT, direction: directionT)
    }
    
    /// Determine the point along this ray at the given parameter
    func extrapolate(_ t: Float) -> SIMD3<Float> {
        return origin + t * direction
    }
    
    /// Determine the parameter corresponding to the point,
    /// assuming it lies on this ray
    func interpolate(_ point: SIMD3<Float>) -> Float {
        return length(point - origin) / length(direction)
    }
}

/// 屏幕上的宽度转换成世界坐标系的宽度
func viewSpaceToWorldScale(_ width: Float,
                           _ model: matrix_float4x4,
                           _ projection: matrix_float4x4,
                           _ viewport: simd_float4) -> Float? {
    
    let p1 = simd_float2(0, viewport.w * 0.5)
    let p1_1 = viewSpaceToHorizontalPlane(p1, model, projection, viewport)?.xy
    
    let p2 = simd_float2(width, viewport.w * 0.5)
    let p2_2 = viewSpaceToHorizontalPlane(p2, model, projection, viewport)?.xy
    
    guard let d1 = p1_1, let d2 = p2_2 else { return nil }
    return simd.distance(d1, d2)
}

/// 屏幕坐标转换成水平面坐标
func viewSpaceToHorizontalPlane(_ point: simd_float2,
                                _ model: matrix_float4x4,
                                _ projection: matrix_float4x4,
                                _ viewport: simd_float4) -> simd_float3? {
    
    let multiply = projection * model
    guard multiply.determinant != 0 else { return nil }
    let inverted = multiply.inverse
    
    let xy = simd_float2(point.x, Float(viewport.w) - point.y)
    let normalised = simd_float2((2 * xy.x / viewport.z - 1),
                                 (2 * (xy.y - viewport.y) / viewport.w - 1))
    let normalisedVectorNear = simd_float4(normalised, -1, 1)
    let normalisedVectorFar = simd_float4(normalised, 1, 1)
    
    var p = inverted * normalisedVectorNear
    let near = p.xyz / p.w
    p = inverted * normalisedVectorFar
    let far = p.xyz / p.w
    
    let k = far.z / near.z
    let x = (k*near.x - far.x)/(k - 1)
    let y = (k*near.y - far.y)/(k - 1)
    return simd_float3(x, y, 0)
}

/// 屏幕坐标转换成世界坐标系的射线
func screenPosToWorldRay(_ point: simd_float2,
                         _ model: matrix_float4x4,
                         _ projection: matrix_float4x4,
                         _ viewport: simd_float4) -> (near: simd_float3, far: simd_float3)? {
    
    let multiply = projection * model
    guard multiply.determinant != 0 else { return nil }
    let inverted = multiply.inverse
    
    let xy = simd_float2(point.x, Float(viewport.w) - point.y)
    let normalised = simd_float2((2 * xy.x / viewport.z - 1),
                                 (2 * (xy.y - viewport.y) / viewport.w - 1))
    let normalisedVectorNear = simd_float4(normalised, -1, 1)
    let normalisedVectorFar = simd_float4(normalised, 1, 1)
    
    var p = inverted * normalisedVectorNear
    let near = p.xyz / p.w
    p = inverted * normalisedVectorFar
    let far = p.xyz / p.w
    return (near, far)
}

func mathProject(_ object: vector_float3,
                 _ model: matrix_float4x4,
                 _ projection: matrix_float4x4,
                 _ viewport: simd_float4) -> vector_float3 {
    
    let v4 = vector4(object, 1.0)
    var v = model * v4
    v = projection * v
    
    v.w = 1.0/v.w
    v.x *= v.w
    v.y *= v.w
    v.z *= v.w
    
    let x = (v.x * 0.5 + 0.5) * viewport.z + viewport.x
    let y = (v.y * 0.5 + 0.5) * viewport.w + viewport.y
    let z = (1.0 + v.z)*0.5
    
    return vector3(x, y, z)
}
/// window.z 最好在0 到 1 之间
func mathUnproject(_ window: vector_float3,
                   _ model: matrix_float4x4,
                   _ projection: matrix_float4x4,
                   _ viewport: vector_float4) -> vector_float3? {
    
    let multiply = projection * model
    guard multiply.determinant != 0 else { return nil }
    let inverted = multiply.inverse
    
    let normalisedVector = simd_float4((2 * window.x / viewport.z - 1),
                                       (2 * (window.y - viewport.y) / viewport.w - 1),
                                       (2 * window.z - 1),
                                       1)
    let point = inverted * normalisedVector
    return point.xyz / point.w
}

func matrix4MakeLookAt(eye: SIMD3<Float>,
                       center: SIMD3<Float>,
                       up: SIMD3<Float>) -> matrix_float4x4
{
    let n = normalize(eye - center)
    let u = normalize(cross(up, n))
    let v = cross(n, u)
    
    return matrix_float4x4(SIMD4<Float>(u.x,            v.x,        n.x,           0),
                           SIMD4<Float>(u.y,            v.y,        n.y,           0),
                           SIMD4<Float>(u.z,            v.z,        n.z,           0),
                           SIMD4<Float>(dot(-u, eye), dot(-v, eye), dot(-n, eye),  1))
}

func contain(position: SIMD2<Float>,
             minBounds: SIMD2<Float>,
             maxBounds: SIMD2<Float>) -> Bool {
    return position.x > minBounds.x && position.x < maxBounds.x && position.y > minBounds.y && position.y < maxBounds.y
}

//https://blog.csdn.net/fengkuangxiaozuo/article/details/110186667
/// 检测线段与矩形是否相交
/// - Parameters:
///   - start: 线段的起始点
///   - end: 线段的结束点
///   - minBounds: 矩形的最小边界点，最小的x和最小的y
///   - maxBounds: 矩形的最大边界点，最大的x和最小的y
func testLineSegmentIntersection(start: SIMD2<Float>,
                                 end: SIMD2<Float>,
                                 minBounds: SIMD2<Float>,
                                 maxBounds: SIMD2<Float>) -> Bool {
    
    let contained = contain(position: start, minBounds: minBounds, maxBounds: maxBounds) || contain(position: end, minBounds: minBounds, maxBounds: maxBounds)
    guard !contained else { return true }
    
    let direction = end - start
    var near = (minBounds - start) / direction
    var far = (maxBounds - start) / direction
    
    if near.x > far.x { swap(&near, &far) }
    
    let n = near.max()
    let f = far.min()
    guard n > 0, f < 1, n < f else { return false }
    return true
}
//https://books.google.co.jp/books?id=BLQdaTmbd-QC&pg=PT125&dq=implementing+object+picking&hl=zh-CN&sa=X&ved=2ahUKEwi-2OzmxcXvAhVcw4sBHZPZD0MQ6AEwAXoECAMQAg#v=onepage&q=implementing%20object%20picking&f=false
/// AABB相交检测 origin: 射线的起始点 direction: 射线的方向
func testRayAABBIntersection(boundingBox: MDLAxisAlignedBoundingBox,
                             ray: Ray) -> Float? {
    
    var near = (boundingBox.minBounds - ray.origin) / ray.direction
    var far = (boundingBox.maxBounds - ray.origin) / ray.direction
    if near.x > far.x { swap(&near, &far) }
    
    let n = near.max()
    let f = far.min()
    guard n > 0, n < f else { return nil }
    return n
}
//http://www.opengl-tutorial.org/cn/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
/// Ray-OBB 相交检测
/// - Parameters:
///   - ray_origin: Ray origin, in world space
///   - ray_direction: Ray direction (NOT target position!), in world space. Must be normalize()'d.
///   - aabb_min: Minimum X,Y,Z coords of the mesh when not transformed at all.
///   - aabb_max: Maximum X,Y,Z coords. Often aabb_min*-1 if your mesh is centered, but it's not always the case.
///   - modelMatrix: Transformation applied to the mesh (which will thus be also applied to its bounding box)
/// - Returns: if the intersected  return true, intersection_distance : distance between ray_origin and the intersection with the OBB
func testRayOBBIntersection(ray: Ray,
                            boundingBox: MDLAxisAlignedBoundingBox,
                            modelMatrix: matrix_float4x4) -> Float? {
    // Intersection method from Real-Time Rendering and Essential Mathematics for Games
    var tMin: Float = 0.0
    var tMax: Float = 100000.0

    let OBBposition_worldspace = modelMatrix[3].xyz
    let delta = OBBposition_worldspace - ray.origin
    
    // Test intersection with the 2 planes perpendicular to the OBB's X axis
    for index in 0...2 {
        let xaxis = modelMatrix[index].xyz
        let e = simd.dot(xaxis, delta)
        let f = simd.dot(ray.direction, xaxis)

        if ( abs(f) > 0.001 ) { // Standard case
            var t1 = (e + boundingBox.minBounds[index]) / f // Intersection with the "left" plane
            var t2 = (e + boundingBox.maxBounds[index]) / f // Intersection with the "right" plane
            // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

            // We want t1 to represent the nearest intersection,
            // so if it's not the case, invert t1 and t2(swap t1 and t2)
            if (t1>t2) { swap(&t1, &t2) }
            // tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
            if (t2 < tMax) { tMax = t2 }
            // tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
            if (t1 > tMin) { tMin = t1 }

            // And here's the trick :
            // If "far" is closer than "near", then there is NO intersection.
            // See the images in the tutorials for the visual explanation.
            if (tMax < tMin ) { return nil }
        } else { // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
            if(-e+boundingBox.minBounds[index] > 0.0 || -e+boundingBox.maxBounds[index] < 0.0) {
                return nil
            }
        }
    }
    return tMin
}
//https://www.cnblogs.com/graphics/archive/2010/08/09/1795348.html
/// Determine whether a ray intersect with a triangle 确定射线和三角形是否相交
/// - Parameters:
///   - orig: origin of the ray
///   - dir: direction of the ray
///   - v0: vertices of triangle
///   - v1: vertices of triangle
///   - v2: vertices of triangle
/// - Returns: if the intersected  return true,  t: weight of the intersection for the ray, u, v: barycentric coordinate of intersection
func intersectTriangle(ray: Ray,
                       v0: SIMD3<Float>,
                       v1: SIMD3<Float>,
                       v2: SIMD3<Float>) -> (Bool, (t: Float, u: Float, v: Float)?) {
    let E1 = v1 - v0
    let E2 = v2 - v0
    
    let P = simd.cross(ray.direction, E2)

    // determinant
    var det = simd.dot(E1, P)

    // keep det > 0, modify T accordingly
    var T: SIMD3<Float>
    if (det > 0) {
        T = ray.origin - v0
    } else {
        T = v0 - ray.origin
        det = -det
    }

    // If determinant is near zero, ray lies in plane of triangle
    if (det < 0.0001) { return (false, nil) }

    // Calculate u and make sure u <= 1
    var u = simd.dot(T,P)
    if( u < 0.0 || u > det ) { return (false, nil) }
    
    let Q = simd.cross(T, E1)

    // Calculate v and make sure u + v <= 1
    var v = simd.dot(ray.direction,Q)
    if (v < 0.0 || u + v > det) { return (false, nil) }

    // Calculate t, scale parameters, ray intersects triangle
    var t = simd.dot(E2,Q)

    let fInvDet = 1.0 / det
    t *= fInvDet
    u *= fInvDet
    v *= fInvDet
    
    return (true, (t, u, v))
}

let π = Float.pi
func radians(_ degrees: Float) -> Float {
    return (degrees / 180) * π
}

func degrees(_ radians: Float) -> Float {
    return (radians / π) * 180
}

extension Float {
    var radiansToDegrees: Float {
        return (self / π) * 180
    }
    var degreesToRadians: Float {
        return (self / 180) * π
    }
}

extension matrix_float4x4 {
    init(translation t: vector_float3) {
        self.init([SIMD4<Float>( 1,     0,      0,      0),
                   SIMD4<Float>( 0,     1,      0,      0),
                   SIMD4<Float>( 0,     0,      1,      0),
                   SIMD4<Float>( t.x,   t.y,    t.z,    1)])
    }
    
    func translated(by t: vector_float3) -> matrix_float4x4 {
        let translateMatrix = matrix_float4x4(translation: t)
        return matrix_multiply(self, translateMatrix)
    }
    
    init(scale s: vector_float3) {
        self.init(SIMD4<Float>( s.x,   0,      0,      0),
                   SIMD4<Float>( 0,     s.y,    0,      0),
                   SIMD4<Float>( 0,     0,      s.z,    0),
                   SIMD4<Float>( 0,     0,      0,      1))
    }
    
    func scaled(by s: vector_float3) -> matrix_float4x4 {
        let scaledMatrix = matrix_float4x4(scale: s)
        return matrix_multiply(self, scaledMatrix)
    }
    
    // MARK:- Rotate
    init(rotationX angle: Float) {
        self.init([1,           0,          0, 0],
                  [0,  cos(angle), sin(angle), 0],
                  [0, -sin(angle), cos(angle), 0],
                  [0,           0,          0, 1])
    }
    
    init(rotationY angle: Float) {
      let matrix = float4x4(
        [cos(angle), 0, -sin(angle), 0],
        [         0, 1,           0, 0],
        [sin(angle), 0,  cos(angle), 0],
        [         0, 0,           0, 1]
      )
      self = matrix
    }
    
    init(rotationZ angle: Float) {
      let matrix = float4x4(
        [ cos(angle), sin(angle), 0, 0],
        [-sin(angle), cos(angle), 0, 0],
        [          0,          0, 1, 0],
        [          0,          0, 0, 1]
      )
      self = matrix
    }
    
    init(rotation angle: vector_float3) {
      let rotationX = float4x4(rotationX: angle.x)
      let rotationY = float4x4(rotationY: angle.y)
      let rotationZ = float4x4(rotationZ: angle.z)
      self = rotationX * rotationY * rotationZ
    }
    
    init(rotationYXZ angle: vector_float3) {
      let rotationX = float4x4(rotationX: angle.x)
      let rotationY = float4x4(rotationY: angle.y)
      let rotationZ = float4x4(rotationZ: angle.z)
      self = rotationY * rotationX * rotationZ
    }
    
    // angle should be in radians
    init(rotationAngle angle: Float, direction: SIMD3<Float>) {
        let c = cos(angle)
        let s = sin(angle)
        let x: Float = direction.x, y: Float = direction.y, z: Float = direction.z
        
        var column0 = SIMD4<Float>(repeating: 0)
        column0.x = x * x + (1 - x * x) * c
        column0.y = x * y * (1 - c) - z * s
        column0.z = x * z * (1 - c) + y * s
        column0.w = 0
        
        var column1 = SIMD4<Float>(repeating: 0)
        column1.x = x * y * (1 - c) + z * s
        column1.y = y * y + (1 - y * y) * c
        column1.z = y * z * (1 - c) - x * s
        column1.w = 0.0
        
        var column2 = SIMD4<Float>(repeating: 0)
        column2.x = x * z * (1 - c) - y * s
        column2.y = y * z * (1 - c) + x * s
        column2.z = z * z + (1 - z * z) * c
        column2.w = 0.0
        
        let column3 = SIMD4<Float>(0, 0, 0, 1)
        self.init(column0, column1, column2, column3)
    }
    
    func rotatedBy(rotationAngle angle: Float, direction: SIMD3<Float>) -> matrix_float4x4 {
        let rotationMatrix = matrix_float4x4(rotationAngle: angle, direction: direction)
        return matrix_multiply(self, rotationMatrix)
    }
    
    func rotated(angle: vector_float3) -> matrix_float4x4 {
        let rotationMatrix = matrix_float4x4(rotation: angle)
        return matrix_multiply(self, rotationMatrix)
    }
    /// 围绕某个点旋转
    func rotated(angle: matrix_float2x3) -> matrix_float4x4 {
        let translate = matrix_float4x4(translation: angle[1])
        let rotationMatrix = matrix_float4x4(rotation: angle[0])
        return self * (translate * rotationMatrix * translate.inverse)
    }
    
    init(projectionFov fov: Float, aspect: Float, nearZ: Float, farZ: Float) {
        let cotan = 1.0 / tanf(fov * 0.5)
        self.init(SIMD4<Float>(cotan/aspect, 0, 0, 0),
                  SIMD4<Float>(0, cotan, 0, 0),
                  SIMD4<Float>(0, 0, (farZ+nearZ)/(nearZ-farZ), -1),
                  SIMD4<Float>(0, 0, (2.0*farZ*nearZ)/(nearZ-farZ), 0))
    }
    // Frustum
    init(frustum left: Float, _ right: Float,
         _ bottom: Float, _ top: Float,
         _ nearZ: Float, _ farZ: Float) {
        
        let ral = right + left
        let rsl = right - left
        let tsb = top - bottom
        let tab = top + bottom
        let fan = farZ + nearZ
        let fsn = farZ - nearZ
        
        self.init([2*nearZ/rsl, 0, 0, 0],
                  [0, 2*nearZ/tsb, 0, 0],
                  [ral/rsl, tab/tsb, -fan/fsn, -1],
                  [0, 0, (-2*farZ*nearZ)/fsn, 0])
    }
    //Ortho
    init(ortho left: Float, _ right: Float,
         _ bottom: Float, _ top: Float,
         _ nearZ: Float, _ farZ: Float) {
        
        let ral = right + left
        let rsl = right - left
        let tab = top + bottom
        let tsb = top - bottom
        let fan = farZ + nearZ
        let fsn = farZ - nearZ
        
        self.init([2.0/rsl, 0, 0, 0],
                  [0, 2.0/tsb, 0, 0],
                  [0, 0, -2.0/fsn, 0],
                  [-ral/rsl, -tab/tsb, -fan/fsn, 1])
    }
    
    var upperLeft: matrix_float3x3 {
        return matrix_float3x3(SIMD3<Float>(columns.0.x, columns.0.y, columns.0.z),
                               SIMD3<Float>(columns.1.x, columns.1.y, columns.1.z),
                               SIMD3<Float>(columns.2.x, columns.2.y, columns.2.z))
    }
    
    func upperLeft3x3() -> matrix_float3x3 {
        return matrix_float3x3(SIMD3<Float>(columns.0.x, columns.0.y, columns.0.z),
                               SIMD3<Float>(columns.1.x, columns.1.y, columns.1.z),
                               SIMD3<Float>(columns.2.x, columns.2.y, columns.2.z))
    }
}

extension matrix_float4x4: CustomReflectable {
    
    public var customMirror: Mirror {
        let c00 = String(format: "%  .4f", columns.0.x)
        let c01 = String(format: "%  .4f", columns.0.y)
        let c02 = String(format: "%  .4f", columns.0.z)
        let c03 = String(format: "%  .4f", columns.0.w)
        
        let c10 = String(format: "%  .4f", columns.1.x)
        let c11 = String(format: "%  .4f", columns.1.y)
        let c12 = String(format: "%  .4f", columns.1.z)
        let c13 = String(format: "%  .4f", columns.1.w)
        
        let c20 = String(format: "%  .4f", columns.2.x)
        let c21 = String(format: "%  .4f", columns.2.y)
        let c22 = String(format: "%  .4f", columns.2.z)
        let c23 = String(format: "%  .4f", columns.2.w)
        
        let c30 = String(format: "%  .4f", columns.3.x)
        let c31 = String(format: "%  .4f", columns.3.y)
        let c32 = String(format: "%  .4f", columns.3.z)
        let c33 = String(format: "%  .4f", columns.3.w)
        
        
        let children = KeyValuePairs<String, Any>(dictionaryLiteral:
                                                  (" ", "\(c00) \(c01) \(c02) \(c03)"),
                                                  (" ", "\(c10) \(c11) \(c12) \(c13)"),
                                                  (" ", "\(c20) \(c21) \(c22) \(c23)"),
                                                  (" ", "\(c30) \(c31) \(c32) \(c33)")
        )
        return Mirror(matrix_float4x4.self, children: children)
    }
}

extension simd_float4: CustomReflectable {
    
    public var customMirror: Mirror {
        let sx = String(format: "%  .4f", x)
        let sy = String(format: "%  .4f", y)
        let sz = String(format: "%  .4f", z)
        let sw = String(format: "%  .4f", w)
        
        let children = KeyValuePairs<String, Any>(dictionaryLiteral:
                                                    (" ", "\(sx) \(sy) \(sz) \(sw)")
        )
        return Mirror(simd_float4.self, children: children)
    }
}

extension simd_float4 {
    
    init(_ xy: SIMD2<Scalar>, _ z: Scalar, _ w: Scalar) {
        self.init(xy.x, xy.y, z, w)
    }
    
    var xyz: SIMD3<Scalar> {
        return SIMD3<Scalar>(x, y, z)
    }
    
    var xy: SIMD2<Scalar> {
        return SIMD2<Scalar>(x, y)
    }
}
extension simd_float3 {
    
    var xy: SIMD2<Scalar> {
        return SIMD2<Scalar>(x, y)
    }
}
