//
//  Shader.metal
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  4:30 PM.
//    
//  

#include <metal_stdlib>
using namespace metal;
#import "Common.h"

struct VertexIn {
    float3 position [[ attribute(0) ]];
    float4 color [[ attribute(1) ]];
    int32_t type [[ attribute(2) ]];
};

struct Sphere {
    float3 position [[ attribute(0) ]];
    float4 color [[ attribute(1) ]];
    float2 textureCoordinate [[ attribute(2) ]];
};

struct VertexTextureIn {
    float3 position [[ attribute(0) ]];
    float4 color [[ attribute(1) ]];
    float2 textureCoordinate [[ attribute(2) ]];
};

struct VertexInModel1 {
    float3 position [[ attribute(0) ]];
    float3 normal [[ attribute(1) ]];
    float2 textureCoordinate [[ attribute(2) ]];
};

struct VertexOut {
    float4 position [[ position ]];
    float4 color;
    float2 textureCoordinate;
    float size [[ point_size ]];
    int type;
};
// 画点
vertex VertexOut vertex_shader(const VertexIn vertexIn [[ stage_in ]],
                               constant Uniforms &uniforms [[buffer(1)]]) {
    
    VertexOut vertexOut;
    float4x4 matrix = uniforms.viewProjectionMatrix * uniforms.modelMatrix;
    vertexOut.position = matrix * float4(vertexIn.position, 1.0);
    vertexOut.color = vertexIn.color;
    vertexOut.size = 4;
    vertexOut.type = vertexIn.type;
    return vertexOut;
}
// 画线
vertex VertexOut vertex_lines_shader(const VertexIn vertexIn [[ stage_in ]],
                                     constant Uniforms &uniforms [[buffer(1)]]) {
    
    VertexOut vertexOut;
    float4x4 matrix = uniforms.viewProjectionMatrix * uniforms.modelMatrix;
    vertexOut.position = matrix * float4(vertexIn.position, 1.0);
    vertexOut.color = vertexIn.color;
    return vertexOut;
}
// 画图片
vertex VertexOut vertex_texture_shader(const VertexTextureIn vertexIn [[ stage_in ]],
                                       constant Uniforms &uniforms [[buffer(1)]]) {
    
    VertexOut vertexOut;
    float4x4 matrix = uniforms.viewProjectionMatrix * uniforms.modelMatrix;
    vertexOut.position = matrix * float4(vertexIn.position, 1.0);
    vertexOut.color = vertexIn.color;
    vertexOut.textureCoordinate = vertexIn.textureCoordinate;
    return vertexOut;
}
// 画模型1
vertex VertexOut vertex_model1_shader(const VertexInModel1 vertexIn [[ stage_in ]],
                                       constant Uniforms &uniforms [[buffer(1)]]) {
    
    VertexOut vertexOut;
    float4x4 matrix = uniforms.viewProjectionMatrix * uniforms.modelMatrix;
    vertexOut.position = matrix * float4(vertexIn.position, 1.0);
    vertexOut.textureCoordinate = vertexIn.textureCoordinate;
    return vertexOut;
}
// 画球
vertex VertexOut vertex_sphere_shader(const Sphere vertexIn [[ stage_in ]],
                                     constant Uniforms &uniforms [[buffer(1)]]) {
    
    VertexOut vertexOut;
    float4x4 matrix = uniforms.viewProjectionMatrix * uniforms.modelMatrix;
    vertexOut.position = matrix * float4(vertexIn.position, 1.0);
    vertexOut.color = vertexIn.color;
    vertexOut.textureCoordinate = vertexIn.textureCoordinate;
    return vertexOut;
}

fragment half4 fragment_shader(VertexOut vertexIn [[ stage_in ]]) {
    if (vertexIn.type == 1) {
        return half4(0.9215686275,1,0,0);
    }
    return half4(vertexIn.color);
}
// 划线
fragment half4 fragment_cube_shader(VertexOut vertexIn [[ stage_in ]],
                                    constant float4 &color [[buffer(1)]]) {
    return half4(color);
}
// 画球
fragment half4 fragment_sphere_shader(VertexOut vertexIn [[ stage_in ]],
                                      sampler sampler2d [[ sampler(0) ]],
                                      texture2d<float> texture [[ texture(0) ]]) {
//    return half4(vertexIn.color);
    float4 color = texture.sample(sampler2d, vertexIn.textureCoordinate);
    return half4(color);
}

fragment half4 fragment_texture_shader(VertexOut vertexIn [[ stage_in ]],
                                       sampler sampler2d [[ sampler(0) ]],
                                       texture2d<float> texture [[ texture(0) ]]) {
    
    float4 color = texture.sample(sampler2d, vertexIn.textureCoordinate);
    return half4(color);
}

struct Position {
    float3 origin;
    /// 分别对应X、Y、Z轴
    float3 size;
};

// 给包含在范围内的顶点设置类型
kernel void vertices_contains(device VertexIn *src [[buffer(0)]],
                              device struct Position &position [[buffer(1)]],
                              device int32_t &type [[buffer(2)]],
                              uint grid_id [[thread_position_in_grid]]) {
    VertexIn vertexIn = src[grid_id];
    float4 p = float4(vertexIn.position, 1.0);
    float x = position.origin.x;
    float y = position.origin.y;
    float z = position.origin.z;
    float maxX = position.origin.x + position.size.x;
    float maxY = position.origin.y + position.size.y;
    float maxZ = position.origin.z + position.size.z;
    if (p.x >= x && p.x <= maxX &&
        p.y >= y && p.y <= maxY &&
        p.z >= z && p.z <= maxZ) {
        src[grid_id].type = type;
    }
}
struct BoundingBox {
    float3 maxBounds;
    float3 minBounds;
};
struct Box {
    float3 maxBounds;
    float3 minBounds;
    matrix_float4x4 matrix;
};

// 改变范围外点的类型
kernel void vertices_contains_hide(device VertexIn *src [[buffer(0)]],
                                   device Box &boundingBox [[buffer(1)]],
                                   uint grid_id [[thread_position_in_grid]]) {
    
    VertexIn vertexIn = src[grid_id];
    float4 p = boundingBox.matrix * float4(vertexIn.position, 1.0);
    
    if (p.x >= boundingBox.minBounds.x && p.x <= boundingBox.maxBounds.x &&
        p.y >= boundingBox.minBounds.y && p.y <= boundingBox.maxBounds.y &&
        p.z >= boundingBox.minBounds.z && p.z <= boundingBox.maxBounds.z) {
        src[grid_id].type = 1;
    }
}

// 改变范围内点的类型
kernel void vertices_contains_change_type(device VertexIn *src [[buffer(0)]],
                                          device Box *boxes [[buffer(1)]],
                                          device int32_t &count [[buffer(2)]],
                                          device int32_t &type [[buffer(3)]],
                                          uint grid_id [[thread_position_in_grid]]) {
    
    VertexIn vertexIn = src[grid_id];
    
    for (int i=0; i<count; i++) {
        float4 p = boxes[i].matrix * float4(vertexIn.position, 1.0);
        if (p.x >= boxes[i].minBounds.x && p.x <= boxes[i].maxBounds.x &&
            p.y >= boxes[i].minBounds.y && p.y <= boxes[i].maxBounds.y &&
            p.z >= boxes[i].minBounds.z && p.z <= boxes[i].maxBounds.z) {
            
            src[grid_id].type = type;
        }
    }
}

struct Cube {
    float3 maxBounds;
    float3 minBounds;
    matrix_float4x4 matrix;
    int32_t id;
    int32_t type;
};

bool contain(float2 position, float2 minBounds, float2 maxBounds) {
    return position.x > minBounds.x && position.x < maxBounds.x && position.y > minBounds.y && position.y < maxBounds.y;
}

bool lineSegmentIntersection(float2 start, float2 end, float2 minBounds, float2 maxBounds) {
    bool contained = contain(start, minBounds, maxBounds) || contain(end, minBounds, maxBounds);
    if (contained) { return true; }
    
    float2 direction = end - start;
    float2 near = (minBounds - start) / direction;
    float2 far = (maxBounds - start) / direction;
    
    if (near.x > far.x) {
        float2 tmp = near;
        near = far;
        far = tmp;
    }
    
    float n = max(near.x, near.y);
    float f = max(far.x, far.y);
    return n > 0 && f < 1 && n < f;
}

// 改变范围内点的类型
kernel void cube_intersect_change_type(device Cube *src [[buffer(0)]],
                                       device float2 *line [[buffer(1)]],
                                       device int32_t &count [[buffer(2)]],
                                       device float &scale [[buffer(3)]],
                                       device int32_t &type [[buffer(4)]],
                                       uint grid_id [[thread_position_in_grid]]) {
    
    Cube cube = src[grid_id];
    float2 minbounds = (cube.matrix * float4(cube.minBounds, 1)).xy;
    float2 maxBounds = (cube.matrix * float4(cube.maxBounds, 1)).xy;
    
    float2x2 matrixRotate = float2x2(0, 1, -1, 0);
    
    float2 start = line[0];
    for (int i=1; i<count; i++) {
        float2 end = line[i];
        
        bool result = lineSegmentIntersection(start, end, minbounds, maxBounds);
        
        if (result) {
            src[grid_id].type = type;
            break;
        }
        
        float2 translation = normalize(matrixRotate * (end - start)) * scale;
        
        float2 start1 = start + translation;
        float2 end1 = end + translation;
        result = lineSegmentIntersection(start1, end1, minbounds, maxBounds);
        if (result) {
            src[grid_id].type = type;
            break;
        }
        
        start1 = start - translation;
        end1 = end - translation;
        result = lineSegmentIntersection(start1, end1, minbounds, maxBounds);
        if (result) {
            src[grid_id].type = type;
            break;
        }
        
        start = end;
    }
}

struct Ray {
    float3 origin;
    float3 direction;
};
// 射线和三角形检测是否相交
kernel void intersectTriangle(device VertexInModel1 *vertexs [[buffer(0)]],
                    device uint32_t *indexs [[buffer(1)]],
                    device Ray &ray [[buffer(2)]],
                    device float &l [[buffer(3)]],
                    uint grid_id [[thread_position_in_grid]]) {
    
    uint32_t i0 = indexs[grid_id];
    uint32_t i1 = indexs[grid_id + 1];
    uint32_t i2 = indexs[grid_id + 2];
    
    VertexInModel1 m0 = vertexs[i0];
    VertexInModel1 m1 = vertexs[i1];
    VertexInModel1 m2 = vertexs[i2];
    
    float3 v0 = float3(m0.position.x, m0.position.y, m0.position.z);
    float3 v1 = float3(m1.position.x, m1.position.y, m1.position.z);
    float3 v2 = float3(m2.position.x, m2.position.y, m2.position.z);
    
    float3 E1 = v1 - v0;
    float3 E2 = v2 - v0;
    
    float3 P = cross(ray.direction, E2);

    // determinant
    float det = dot(E1, P);

    // keep det > 0, modify T accordingly
    float3 T;
    if (det > 0) {
        T = ray.origin - v0;
    } else {
        T = v0 - ray.origin;
        det = -det;
    }
    l = -2;
    // If determinant is near zero, ray lies in plane of triangle
    if (det < 0.0001) { return; }

    l = -3;
    // Calculate u and make sure u <= 1
    float u = dot(T,P);
    if ( u < 0.0 || u > det ) { return; }
    
    float3 Q = cross(T, E1);

    l = -4;
    // Calculate v and make sure u + v <= 1
    float v = dot(ray.direction,Q);
    if (v < 0.0 || u + v > det) { return; }

    // Calculate t, scale parameters, ray intersects triangle
    float t = dot(E2,Q);

    float fInvDet = 1.0 / det;
    t *= fInvDet;
//    u *= fInvDet;
//    v *= fInvDet;
    
    l = t;
}
