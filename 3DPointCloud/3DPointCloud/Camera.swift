//
//  Camera.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  3:37 PM.
//    
//  

import MetalKit

class Camera: Node {
    /// 弧度
    var fovRadians: Float = radians(65) {
        didSet {
            updateProjectionMatrix()
        }
    }
    var aspect: Float = 1 {
        didSet {
            updateProjectionMatrix()
        }
    }
    let nearZ: Float = 0.01
    let farZ: Float = 5000
    var viewPortWidth: Float = 100 {
        didSet {
            updateProjectionMatrix()
        }
    }
    
    fileprivate(set) var isProjection = false {
        didSet {
            updateProjectionMatrix()
        }
    }
    fileprivate var _projectionMatrix: matrix_float4x4 = matrix_identity_float4x4
    var projectionMatrix: matrix_float4x4 {
        return _projectionMatrix
    }
    func updateProjectionMatrix() {
        if isProjection {
        _projectionMatrix = matrix_float4x4(projectionFov: fovRadians,
                                            aspect: aspect,
                                            nearZ: nearZ,
                                            farZ: farZ)
        } else {
            _projectionMatrix = matrix_float4x4(ortho: -viewPortWidth, viewPortWidth, -viewPortWidth/aspect, viewPortWidth/aspect, -500, 500)
        }
        updateviewProjectionMatrix()
    }
    
    fileprivate var _viewMatrix = matrix_identity_float4x4
    var viewMatrix: matrix_float4x4 {
        return _viewMatrix
    }
    
    fileprivate(set) var viewProjectionMatrix: matrix_float4x4 = matrix_identity_float4x4
    func updateviewProjectionMatrix() {
        viewProjectionMatrix = projectionMatrix * viewMatrix
    }
    
    override init(mtlDevice: MTLDevice) {
        super.init(mtlDevice: mtlDevice)
        if isProjection {
            // 透视投影矩阵
            _projectionMatrix = matrix_float4x4(projectionFov: fovRadians,
                                                aspect: aspect,
                                                nearZ: nearZ,
                                                farZ: farZ)
        } else {
            // 正视投影矩阵
            _projectionMatrix = matrix_float4x4(ortho: -viewPortWidth,
                                                viewPortWidth,
                                                -viewPortWidth/aspect,
                                                viewPortWidth/aspect,
                                                -500,
                                                500)
        }
        _viewMatrix = modelMatrix.inverse
        viewProjectionMatrix = _projectionMatrix * _viewMatrix
    }
    
    func zoom(delta: Float) {}
    func rotate(delta: SIMD3<Float>) {}
    func translate(delta: SIMD2<Float>) {}
}
/// 第三视角
class ArcballCamera: Camera {
    /// 俯视角 0度到90度之间 0...pi/2
    private var matrixRotateX = float4x4(rotationX: radians(0))
    private var depressionAngle: Float = radians(0) {
        didSet {
            matrixRotateX = float4x4(rotationX: depressionAngle)
            updateViewMatrix()
        }
    }
    
    private var matrixTarget = float4x4(translation: SIMD3<Float>(0, 0, 0))
    private var target: SIMD3<Float> = [0, 0, 0] {
        didSet {
            matrixTarget = float4x4(translation: target)
            updateViewMatrix()
        }
    }
    // 一下几个值如果改动，统一改了
    let minDistance: Float = 1
    let maxDistance: Float = 500
    let total: Float = 500 - 1
    var matrixDistance = float4x4(translation: SIMD3<Float>(0, 0, 250))
    private var distance: Float = 250 {
        didSet {
            matrixDistance = float4x4(translation: SIMD3<Float>(0, 0, distance))
            updateViewMatrix()
        }
    }
    
    private(set) var matrixRotateZ = float4x4(rotationZ: 0)
    private(set) var rotationZ: Float = 0 {
        didSet {
            rotation.z = rotationZ
            matrixRotateZ = float4x4(rotationZ: rotationZ)
            updateViewMatrix()
        }
    }
    
    override var modelMatrix: matrix_float4x4 {
        return _modelMatrix
    }
    override var viewMatrix: float4x4 {
        return _viewMatrix
    }
    private var _modelMatrix = matrix_identity_float4x4
    
    override init(mtlDevice: MTLDevice) {
        super.init(mtlDevice: mtlDevice)
        
        updateViewMatrix()
    }
    
    private func updateViewMatrix() {
        _modelMatrix = matrixTarget * matrixRotateZ * matrixRotateX * matrixDistance
        _viewMatrix = _modelMatrix.inverse
        updateviewProjectionMatrix()
    }
    
    override func zoom(delta: Float) {
        let scale = (distance - minDistance) / total * 3 + 0.01
        if isProjection {
            let result = distance + delta * scale * -20
            switch result {
            case -Float.infinity..<minDistance:
                distance = minDistance
            case maxDistance..<Float.infinity:
                distance = maxDistance
            default:
                distance = result
            }
        } else {
            let result = viewPortWidth + delta * scale * -5
            switch result {
            case -Float.infinity..<2:
                viewPortWidth = 2
            case 125..<Float.infinity:
                viewPortWidth = 125
            default:
                viewPortWidth = result
            }
        }
    }
    
    override func rotate(delta: SIMD3<Float>) {
        let sensitivity: Float = -0.005
        rotationZ += delta.x * sensitivity
        
        var angle = depressionAngle + delta.y * sensitivity
        angle = max(min(Float.pi*0.5, angle), 0)
        depressionAngle = angle
    }
    
    override func translate(delta: SIMD2<Float>) {
        if isProjection {
            let scale = (distance - minDistance) / total * 3 * 6
            let matrixRotateZ = float4x4(rotationZ: -rotationZ)
            target += (matrixRotateZ.inverse * SIMD4<Float>(-delta * scale, 0, 1)).xyz
        } else {
            let scale = (distance - minDistance) / total * 3
            let matrixRotateZ = float4x4(rotationZ: -rotationZ)
            target += (matrixRotateZ.inverse * SIMD4<Float>(-delta * scale, 0, 1)).xyz
        }
    }
    
    func setTarget(position: SIMD3<Float>) {
        target = position
    }
    /// 弧度
    func setRotationZ(rotation: Float) {
        rotationZ = rotation
    }
    func setDepressionAngle(angle: Float) {
        let angle = max(min(Float.pi*0.5, angle), 0)
        depressionAngle = angle
    }
    
    func setDistance(distance s: Float) {
        let s = max(min(maxDistance, s), minDistance)
        distance = s
    }
}
/// 第一视角
class LookAtCamera: Camera {
    
    private var front: SIMD3<Float> = normalize(vector3(0, 1, 0))
    private var up: SIMD3<Float> = normalize(vector3(0, 0, 1))
    private var right: SIMD3<Float> {
        return normalize(cross(front, up))
    }
    
    override init(mtlDevice: MTLDevice) {
        super.init(mtlDevice: mtlDevice)
        
        updateCameraVectors()
    }
    
    override func rotate(delta: SIMD3<Float>) {
        let delta = delta * -0.05
        let inRadians: Float
        let direction: SIMD3<Float>
        if delta.z != 0 {
            direction = front
            inRadians = -delta.z * 0.3
        } else if abs(delta.x) > abs(delta.y) {
            direction = up
            inRadians = delta.x * Float.pi / 90
        } else {
            direction = right
            inRadians = delta.y * Float.pi / 90
        }
        
        let axis    = normalize(direction)
        let ct      = cosf(inRadians)
        let st      = sinf(inRadians)
        let ci      = 1 - ct
        let x       = axis.x
        let y       = axis.y
        let z       = axis.z
        let mat = matrix_float3x3(
            [ct + x * x * ci, y * x * ci + z * st, z * x * ci - y * st ],
            [x * y * ci - z * st, ct + y * y * ci, z * y * ci + x * st],
            [x * z * ci + y * st, y * z * ci - x * st, ct + z * z * ci ])
        
        front = normalize(front * mat)
        up = normalize(up * mat)
        updateCameraVectors()
    }
    
    override func zoom(delta: Float) {
        
        position += front * delta
        
        updateCameraVectors()
    }
    
    override func translate(delta: SIMD2<Float>) {
        
        if abs(delta.x) > abs(delta.y) {
            position -= right * delta.x
        } else {
            position -= up * delta.y
        }
        
        updateCameraVectors()
    }
    
    private func updateCameraVectors() {
        _viewMatrix = matrix4MakeLookAt(eye: position, center: position + front, up: up)
        updateviewProjectionMatrix()
    }
}
