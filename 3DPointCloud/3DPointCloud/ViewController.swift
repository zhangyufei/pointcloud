//
//  ViewController.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  2:10 PM.
//    
//  

import UIKit
import MetalKit

class ViewController: UIViewController {

    var mtlView: MTKView?
    var renderer: Renderer?
    var scene: PointCloudScene?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        
        let mtlView = MTKView(frame: view.bounds)
        mtlView.colorPixelFormat = .bgra8Unorm
        view.addSubview(mtlView)
        guard let renderer = Renderer(view: mtlView) else { return }
        let scene = PointCloudScene(device: renderer.device, size: view.bounds.size, view: mtlView, fileName: "1604131731095113")
        renderer.scene = scene
        
        self.mtlView = mtlView
        self.renderer = renderer
        self.scene = scene
    }
}

class WBSegmented: UIView {
    
    private let leftButton = UIButton(type: .custom)
    private let rightButton = UIButton(type: .custom)
    
    var isSelectedLeft: Bool {
        return leftButton.isSelected
    }
    var handlebuttonClick: ((Bool) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(red: 144/255.0, green: 167/255.0, blue: 178/255.0, alpha: 1)
        layer.cornerRadius = 16
        addSubview(leftButton)
        addSubview(rightButton)
        
        leftButton.frame = CGRect(x: 3, y: 3, width: 26, height: 26)
        rightButton.frame = CGRect(x: leftButton.frame.maxX + 3, y: 3, width: 26, height: 26)
        
        leftButton.setImage(UIImage(named: "wb_audioRecord_play_clear"), for: .normal)
        leftButton.setImage(UIImage(named: "wb_audioRecord_play"), for: .selected)
        
        rightButton.setImage(UIImage(named: "wb_audioRecord_pause_clear"), for: .normal)
        rightButton.setImage(UIImage(named: "wb_audioRecord_pause"), for: .selected)
        
        leftButton.addTarget(self, action: #selector(handleLeftButtonClick), for: [.touchUpInside, .touchDragEnter])
        rightButton.addTarget(self, action: #selector(handleRightButtonClick), for: [.touchUpInside, .touchDragEnter])
        
        leftButton.isSelected = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleLeftButtonClick() {
        guard !leftButton.isSelected else { return }
        rightButton.isSelected = false
        leftButton.isSelected = true
        handlebuttonClick?(true)
    }
    
    @objc private func handleRightButtonClick() {
        guard !rightButton.isSelected else { return }
        rightButton.isSelected = true
        leftButton.isSelected = false
        handlebuttonClick?(false)
    }
}
