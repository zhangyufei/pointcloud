//
//  Scene.swift
//  3DPointCloud
//
//  路漫漫其修远兮 吾将上下而求索
//  Created by 张玉飞 on 2021/1/13  3:26 PM.
//    
//  

import MetalKit

class Scene: Node {
    
    var size: CGSize {
        didSet {
            if oldValue != size {
                camera.aspect = Float(size.width / size.height)
            }
        }
    }
    var projectionMatrix = matrix_identity_float4x4
    
    lazy var camera: Camera = {
        let camera = ArcballCamera(mtlDevice: device)
        
//        let camera = LookAtCamera(mtlDevice: device)
//        camera.translate(delta: SIMD2<Float>(0, -1.5))
        return camera
    }()
    var view: MTKView
    
    init(device: MTLDevice, size: CGSize, view: MTKView) {
//        if device.supportsTextureSampleCount(4) {
//            view.sampleCount = 4
//        }
        self.size = size
        self.view = view
        super.init(mtlDevice: device)
        camera.aspect = Float(size.width / size.height)
    }
    
    func render(commandEncoder: MTLRenderCommandEncoder) {
        
        let uniforms = Uniforms(modelMatrix: modelMatrix,
                                viewProjectionMatrix: camera.viewProjectionMatrix)
        
        for child in children {
            child.render(commandEncoder: commandEncoder, uniforms: uniforms)
        }
    }
    // MARK: - 手势相关
    var beganScale: CGFloat = 1
    /// 平移手势
    lazy var panGesture: UIPanGestureRecognizer = {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePanGestureRecognizer(_:)))
        return pan
    }()
    
    lazy var pinchGesture: UIPinchGestureRecognizer = {
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGestureRecognizer(_:)))
        return pinch
    }()
    
    lazy var rotationGesture: UIRotationGestureRecognizer = {
        let rotation = UIRotationGestureRecognizer(target: self, action: #selector(handleRotationGestureRecognizer(_:)))
        return rotation
    }()
    
    lazy var pressGesture: UILongPressGestureRecognizer = {
        let press = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGestureRecognizer(_:)))
        
        return press
    }()
    
    func setGestureRecognizer() {
        view.addGestureRecognizer(panGesture)
        view.addGestureRecognizer(pinchGesture)
        view.addGestureRecognizer(rotationGesture)
        view.addGestureRecognizer(pressGesture)
    }
    /// 平移手势处理
    @objc func handlePanGestureRecognizer(_ pan: UIPanGestureRecognizer) {
        let translation = pan.translation(in: view)
        
        if pan.numberOfTouches == 2 {
            let scale: CGFloat = 1/30
            let delta = vector2(Float(translation.x * scale),
                                Float(-translation.y * scale))
            camera.translate(delta: delta)
        } else {
            var delta = vector3(Float(translation.x),
                                Float(translation.y), 0)
            let location = pan.location(in: view)
            if location.y < size.height * 0.5 {
                delta.x = -delta.x
            }
            camera.rotate(delta: delta)
        }
        
        pan.setTranslation(.zero, in: pan.view)
    }
    
    /// 旋转手势处理
    @objc func handleRotationGestureRecognizer(_ rotation: UIRotationGestureRecognizer) {
        
        switch rotation.state {
        case .began:
            break
        case .changed:
            let delta = vector3(0, 0, Float(rotation.rotation))
            camera.rotate(delta: delta)
        default:
            break
        }
    }
    /// 缩放处理
    @objc func handlePinchGestureRecognizer(_ pinch: UIPinchGestureRecognizer) {
        switch pinch.state {
        case .began:
            beganScale = 1
        case .changed:
            let delta = Float(pinch.scale - beganScale) * 3
            camera.zoom(delta: delta)
            beganScale = pinch.scale
        case .ended:
            beganScale = 1
        default:
            break
        }
    }
    
    /// 处理长按手势
    @objc func handleLongPressGestureRecognizer(_ press: UILongPressGestureRecognizer) {
        
    }
}

extension Scene {

    
}
